package com.devfatani.webvuw

import android.content.Context
import android.graphics.Bitmap
import android.os.Build
//import android.support.annotation.RequiresApi
import android.annotation.TargetApi
import androidx.annotation.RequiresApi
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import android.app.Activity
import android.content.ActivityNotFoundException
import android.view.View
import android.view.MotionEvent
import android.webkit.WebView
import android.util.Log
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.platform.PlatformView
//import android.support.v4.widget.SwipeRefreshLayout
import android.content.Intent
import android.view.ViewGroup
import android.webkit.ValueCallback
import android.webkit.WebResourceRequest
import android.webkit.WebViewClient
import android.webkit.WebSettings
import android.widget.LinearLayout
import android.webkit.WebChromeClient
import android.webkit.ConsoleMessage
import android.net.Uri

// extra class
import android.webkit.JavascriptInterface
import org.jetbrains.anko.runOnUiThread
import org.jetbrains.anko.toast
import io.flutter.plugin.common.EventChannel
// import com.tencent.mm.opensdk.modelpay.PayReq
import android.database.Cursor;
import android.provider.OpenableColumns
import io.flutter.plugin.common.PluginRegistry
import android.provider.MediaStore
import java.util.ArrayList
import java.util.HashMap
import java.io.File
import java.util.Date
import java.io.IOException
import java.text.SimpleDateFormat
import androidx.core.content.FileProvider

enum class FlutterMethodName {
    loadUrl,
    canGoBack,
    canGoForward,
    goBack,
    goForward,
    stopLoading,
    evaluateJavascript,
    reload,
    loadHtml
}

class WebVuw internal constructor(
    context: Context,
    messenger: BinaryMessenger,
    id: Int,
    params: Map<*, *>,
    activity: Activity
) :
        PlatformView,
        MethodCallHandler,
        SwipeRefreshLayout.OnRefreshListener,
        PluginRegistry.ActivityResultListener,
        EventChannel.StreamHandler {

    companion object {

        private var mUploadMessage: ValueCallback<Uri>? = null
        private var mUploadMessage5: ValueCallback<Array<Uri>>? = null
        //private var mUploadMessageArray: ValueCallback<Array<Uri>>? = null
        private var fileUri: Uri? = null
        private var videoUri: Uri? = null

        const val TAG = "WebVuw"
        // 日志是否打印的标识
        val flag: Boolean = true
        // PRAMS NAME
        const val INITIAL_URL = "initialUrl"
        const val HEADER = "header"
        const val USER_AGENT = "userAgent"
        const val CHANNEL_NAME = "plugins.devfatani.com/web_vuw_"
        const val WEB_VUW_EVENT = "web_vuw_events_"
        const val EVENT = "event"
        const val URL = "url"
        const val HTML = "html"
        const val PULL_TO_REFRESH = "pullToRefresh"
        const val ENABLE_JAVA_SCRIPT = "enableJavascript"
        const val ENABLE_LOCAL_STORAGE = "enableLocalStorage"
        const val INJECT_URL = "http://tata.baojiawangluo.com/static/js/tyinject.js"
        const val INJECT_URL_SSL = "https://tata.baojiawangluo.com/static/js/tyinject.js"
        const val INJECT_CSS = "http://tata.baojiawangluo.com/static/css/tyinject.css"
        const val INJECT_CSS_SSL = "https://tata.baojiawangluo.com/static/css/tyinject.css"
        const val VERBOSE = 2
        const val DEBUG = 3
        const val INFO = 4
        const val WARN = 5
        const val ERROR = 6
        const val ASSERT = 7
        const val FILECHOOSER_RESULTCODE = 5173
        const val FILECHOOSER_RESULTCODE_FOR_ANDROID_5 = 5174
        const val FILE_CHOOSER_RESULT_CODE = 10000;
    }


    private var injected: Boolean = false
    private var mContext: Context? = context
    private var isPullToRefreshAllowed = false
    private val linearLay = LinearLayout(context)

    private var swipeRefresh: SwipeRefreshLayout? = null
    private val webVuw: WebView = WebView(context)
    private val methodChannel: MethodChannel

    private var eventSinkNavigation: EventChannel.EventSink? = null
    private var INJECT_SCRIPT: String = "window.sendEvent=function(data){if(typeof(data)=='string'){var msg = data;}else{var msg=JSON.stringify(data);};if(typeof window.TuanyouService!='undefined') {window.TuanyouService.postMessage(msg);}else if (window.webkit.messageHandlers.TuanyouService) {window.webkit.messageHandlers.TuanyouService.postMessage(msg);}}"

    private val mainActivity: Activity = activity
    private var consoleMessageOK: Boolean = false

    init {
        if (params[INITIAL_URL] != null) {
            val initialURL = params[INITIAL_URL] as String

            if (params[HEADER] != null) {
                val header = params[HEADER] as Map<String, String>
                webVuw.loadUrl(initialURL, header)
            } else webVuw.loadUrl(initialURL)

            if (params[USER_AGENT] != null) {
                val userAgent = params[USER_AGENT] as String
                webVuw.settings.userAgentString = userAgent
            }
        } else if (params[HTML] != null) {
            val html = params[HTML] as String
            webVuw.loadDataWithBaseURL("", html, "text/html", "UTF-8", "")
        }

        jsSettings(params)

        val self = this@WebVuw

        webVuw.requestFocus()
        // 解决输入框不能输入的bug
        /*
        webVuw.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                when (event?.action) {
                        MotionEvent.ACTION_DOWN, MotionEvent.ACTION_UP -> {
                            v?.requestFocusFromTouch()
                        }
                        MotionEvent.ACTION_MOVE -> {
                        }
                        MotionEvent.ACTION_CANCEL -> {
                        }
                }

                return v?.onTouchEvent(event) ?: true
            }
        })
        */
        webVuw.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                try {
                    view!!.loadUrl("javascript:" + INJECT_SCRIPT)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                    log("e", ex.toString())
                }
                if (self.eventSinkNavigation != null && url != null) {
                    val result = HashMap<String, String>().apply {
                        put(EVENT, "onPageStarted")
                        put(URL, url)
                    }
                    self.eventSinkNavigation!!.success(result)
                }
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                var jsString: String = "var newscript = document.createElement('script');newscript.src='" + INJECT_URL + "';document.getElementsByTagName('head')[0].appendChild(newscript);(function() {var parent = document.getElementsByTagName('head').item(0);var style = document.createElement('link');style.type = 'text/css';style.href = '" + INJECT_CSS + "';parent.appendChild(style);})()"
                if(url?.startsWith("https://")==true){
                    jsString = "var newscript = document.createElement('script');newscript.src='" + INJECT_URL_SSL + "';document.getElementsByTagName('head')[0].appendChild(newscript); (function() {var parent = document.getElementsByTagName('head').item(0);var style = document.createElement('link');style.type = 'text/css';style.href = '" + INJECT_CSS_SSL + "';parent.appendChild(style);})()"
                }
                try {
                    log("i", "准备注入js" + jsString)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        log("i", "开始注入js" + jsString)
                        log("i", view == null)
                        view!!.evaluateJavascript(jsString, object : ValueCallback<String> {
                            override fun onReceiveValue(value: String?) {
                                log("i", "注入js返回" + value)
                                injected = true
                            }
                        })
                    } else {
                        log("i", "5.0以下注入js" + jsString)
                        view!!.loadUrl("javascript:" + jsString)
                        injected = true
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                    log("e", ex.toString())
                }
                if (self.eventSinkNavigation != null && url != null) {
                    val result = HashMap<String, String>().apply {
                        put(EVENT, "onPageFinished")
                        put(URL, url)
                    }
                    self.eventSinkNavigation!!.success(result)
                }
            }

            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                if (request != null && self.eventSinkNavigation != null) {
                    val nxtUrl = request.url.toString()
                    val result = HashMap<String, String>().apply {
                        put(EVENT, "shouldOverrideUrlLoading")
                        put(URL, nxtUrl)
                    }
                    self.eventSinkNavigation!!.success(result)
                }
                return super.shouldOverrideUrlLoading(view, request)
            }
        }

        if (params[PULL_TO_REFRESH] != null) {
            val pullToRefresh = params[PULL_TO_REFRESH] as Boolean
            if (pullToRefresh) {
                isPullToRefreshAllowed = true
                val swipeRefresh = SwipeRefreshLayout(context)
                linearLay.orientation = LinearLayout.VERTICAL
                swipeRefresh.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT)
                webVuw.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT)
                linearLay.addView(swipeRefresh)
                swipeRefresh.addView(webVuw)
                swipeRefresh.setOnRefreshListener(this@WebVuw)
                self.swipeRefresh = swipeRefresh
            }
        }

        EventChannel(messenger, "$WEB_VUW_EVENT$id").setStreamHandler(this@WebVuw)

        methodChannel = MethodChannel(messenger, "$CHANNEL_NAME$id")
        methodChannel.setMethodCallHandler(this@WebVuw)

        webVuw.setWebChromeClient(object : WebChromeClient() {

            fun openFileChooser(uploadMsg: ValueCallback<Uri>) {
                this.openFileChooser(uploadMsg, "", "")
            }

            // For Android >= 3.0
            fun openFileChooser(
                uploadMsg: ValueCallback<Uri>,
                acceptType: String
            ) {
                if (WBH5FaceVerifySDK.getInstance().recordVideoForApiBelow21(uploadMsg,
                        acceptType, mainActivity)) {
                            return
                        }
                this.openFileChooser(uploadMsg, acceptType, "")
            }

            // For Android >= 4.1
            fun openFileChooser(
                uploadMsg: ValueCallback<Uri>,
                acceptType: String,
                capture: String
            ) {
                if (WBH5FaceVerifySDK.getInstance().recordVideoForApiBelow21(uploadMsg,
                        acceptType, mainActivity)) {
                            return
                        }
                mUploadMessage = uploadMsg
                var i = Intent(Intent.ACTION_GET_CONTENT)
                i.addCategory(Intent.CATEGORY_OPENABLE)
                i.setType("*/*")
                activity.startActivityForResult(Intent.createChooser(i, "File Browser"),
                        FILECHOOSER_RESULTCODE)
            }

            // For Lollipop 5.0+ Devices
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onShowFileChooser(
                mWebView: WebView,
                filePathCallback: ValueCallback<Array<Uri>>,
                fileChooserParams: WebChromeClient.FileChooserParams
            ): Boolean {
                Log.i("[WebView]", String.format("开始上传文件"))

                if (WBH5FaceVerifySDK.getInstance().recordVideoForApi21(mWebView,
                        filePathCallback, activity, fileChooserParams)) {
                            return true
                        }
                if (mUploadMessage5 != null) {
                    mUploadMessage5?.onReceiveValue(null)
                    mUploadMessage5 = null
                }

                val acceptTypes = getSafeAcceptedTypes(fileChooserParams)
                val intentList = ArrayList<Intent>()
                fileUri = null
                videoUri = null

                Log.i("[WebView]", String.format("文件类型%s ",acceptTypes))

                if (acceptsImages(acceptTypes)) {
                    Log.i("[WebView]", String.format("图片文件"))
                    val takePhotoIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    fileUri = getOutputFilename(MediaStore.ACTION_IMAGE_CAPTURE)
                    takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
                    intentList.add(takePhotoIntent)
                    Log.i("[WebView]", "fileUri $fileUri ")
                }

                if (acceptsVideo(acceptTypes)) {
                    Log.i("[WebView]", String.format("视频文件"))
                    val takeVideoIntent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
                    videoUri = getOutputFilename(MediaStore.ACTION_VIDEO_CAPTURE)
                    takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, videoUri)
                    intentList.add(takeVideoIntent)
                }

                val contentSelectionIntent: Intent
                if (Build.VERSION.SDK_INT >= 21) {
                    val allowMultiple = fileChooserParams.getMode() == WebChromeClient.FileChooserParams.MODE_OPEN_MULTIPLE
                    contentSelectionIntent = fileChooserParams.createIntent()
                    contentSelectionIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, allowMultiple)
                } else {
                    contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
                    contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
                    contentSelectionIntent.setType("*/*")
                }
                val intentArray = intentList.toTypedArray()

                mUploadMessage5 = filePathCallback
                val chooserIntent = Intent(Intent.ACTION_CHOOSER)
                chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)

                Log.i("[WebView]", String.format("开始上传"))
                try {
                    activity.startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE)
                } catch (e: ActivityNotFoundException) {
                    mUploadMessage5 = null
                    return false
                }
                return true

                /*
                mUploadMessage5 = filePathCallback
                var intent = fileChooserParams.createIntent()
                try {
                    activity.startActivityForResult(intent,
                            FILECHOOSER_RESULTCODE_FOR_ANDROID_5);
                } catch (e: ActivityNotFoundException) {
                    mUploadMessage5 = null
                    return false
                }
                return true
                */
            }

            private fun getOutputFilename(intentType: String): Uri {
                var prefix = ""
                var suffix = ""

                if (intentType === MediaStore.ACTION_IMAGE_CAPTURE) {
                    prefix = "image-"
                    suffix = ".jpg"
                } else if (intentType === MediaStore.ACTION_VIDEO_CAPTURE) {
                    prefix = "video-"
                    suffix = ".mp4"
                }

                val packageName = mContext?.getPackageName()
                var capturedFile: File? = null
                try {
                    capturedFile = createCapturedFile(prefix, suffix)
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                return FileProvider.getUriForFile(context, packageName + ".fileProvider", capturedFile as File)
            }

            @Deprecated("onConsoleMessage message:String,  lineNumber:Int,  sourceID:String is Deprecated")
            override fun onConsoleMessage(message: String, lineNumber: Int, sourceID: String) {
                Log.i("[WebView]", String.format("sourceID: %s lineNumber: %n message: %s", sourceID,
                lineNumber, message))
                self.consoleMessageOK = true
                super.onConsoleMessage(message, lineNumber, sourceID)
            }

            override fun onConsoleMessage(consoleMessage: ConsoleMessage): Boolean {
                var message: String = consoleMessage.message()
                var lineNumber: Int = consoleMessage.lineNumber()
                var sourceID: String = consoleMessage.sourceId()
                var messageLevel: String = consoleMessage.message()
                self.consoleMessageOK = true
                Log.i("[WebView]", String.format("[%s] sourceID: %s lineNumber: %n message: %s",
                        messageLevel, sourceID, lineNumber, message))

                return super.onConsoleMessage(consoleMessage)
            }

            @Throws(IOException::class)
            private fun createCapturedFile(prefix: String, suffix: String): File {
                val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
                val imageFileName = prefix + "_" + timeStamp
                val storageDir = context.getExternalFilesDir(null)
                return File.createTempFile(imageFileName, suffix, storageDir)
            }

            private fun acceptsImages(types: Array<String>): Boolean {
                return isArrayEmpty(types) || arrayContainsString(types, "image")
            }

            private fun acceptsVideo(types: Array<String>): Boolean {
                return isArrayEmpty(types) || arrayContainsString(types, "video")
            }

            private fun arrayContainsString(array: Array<String>, pattern: String): Boolean {
                for (content in array) {
                    if (content.contains(pattern)) {
                        return true
                    }
                }
                return false
            }

            private fun isArrayEmpty(arr: Array<String>): Boolean {
                // when our array returned from getAcceptTypes() has no values set from the
                // webview
                // i.e. <input type="file" />, without any "accept" attr
                // will be an array with one empty string element, afaik
                return arr.size == 0 || arr.size == 1 && arr[0].length == 0
            }

            private fun getSafeAcceptedTypes(params: WebChromeClient.FileChooserParams): Array<String> {

                // the getAcceptTypes() is available only in api 21+
                // for lower level, we ignore it
                return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    params.getAcceptTypes()
                } else arrayOf()
            }
        })

        if (consoleMessageOK == false) {
            Log.i("[WebView]", "onConsoleMessage not call")
            webVuw.addJavascriptInterface(Console(), "console")
        }
    }

    private fun log(type: String, any: Any) {
        if (!flag) return
        var msg = any.toString()
        when (type) {
            "d" -> Log.d(TAG, msg)
            "i" -> Log.i(TAG, msg)
            "w" -> Log.w(TAG, msg)
            "e" -> Log.e(TAG, msg)
        }
    }

    private fun jsSettings(params: Map<*, *>) {
        val isJavaScriptEnabled = params[ENABLE_JAVA_SCRIPT] as Boolean
        val isLocalStorageEnabled = params[ENABLE_LOCAL_STORAGE] as Boolean
        webVuw.settings.javaScriptEnabled = isJavaScriptEnabled
        webVuw.settings.domStorageEnabled = isLocalStorageEnabled
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webVuw.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW)
        }

        // kotlin与H5通信方式1：H5调用Kotlin方法
        // 设置kotlin与H5的通信桥梁类
        webVuw.addJavascriptInterface(this, "TuanyouService")
    }

    override fun getView(): View {
        return if (isPullToRefreshAllowed) linearLay else webVuw
    }

    override fun onMethodCall(methodCall: MethodCall, result: Result) {
        when (FlutterMethodName.valueOf(methodCall.method)) {
            FlutterMethodName.loadUrl -> loadUrl(methodCall, result)
            FlutterMethodName.canGoBack -> canGoBack(methodCall, result)
            FlutterMethodName.canGoForward -> canGoForward(methodCall, result)
            FlutterMethodName.goBack -> goBack(methodCall, result)
            FlutterMethodName.goForward -> goForward(methodCall, result)
            FlutterMethodName.stopLoading -> stopLoading(methodCall, result)
            FlutterMethodName.evaluateJavascript -> evaluateJavaScript(methodCall, result)
            FlutterMethodName.reload -> reload(methodCall, result)
            FlutterMethodName.loadHtml -> loadHtml(methodCall, result)
        }
    }

    private fun loadHtml(methodCall: MethodCall, result: Result) {
        (methodCall.arguments as String).let { html ->
            webVuw.loadDataWithBaseURL("", html, "text/html", "UTF-8", "")
        }
    }

    private fun evaluateJavaScript(methodCall: MethodCall, result: Result) {
        val jsString = methodCall.arguments as String
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webVuw.evaluateJavascript(jsString) { value -> result.success(value) }
        }
    }

    private fun loadUrl(methodCall: MethodCall, result: Result) {
        val url = methodCall.arguments as String
        /**
        * 对 WebSettings 进行设置:添加 ua 字段和适配 h5 页面布局等 * @param mWebView 第三方的 WebView 对象
        * @param context 第三方上下文
        */
        WBH5FaceVerifySDK.getInstance().setWebViewSettings(webVuw, mContext as Context)
        webVuw.loadUrl(url)
        result.success(null)
    }

    private fun reload(methodCall: MethodCall, result: Result) {
        webVuw.reload()
    }

    private fun stopLoading(methodCall: MethodCall, result: Result) {
        webVuw.stopLoading()
        result.success(null)
    }

    private fun canGoBack(methodCall: MethodCall, result: Result) {
        result.success(webVuw.canGoBack())
    }

    private fun canGoForward(methodCall: MethodCall, result: Result) {
        result.success(webVuw.canGoForward())
    }

    private fun goBack(methodCall: MethodCall, result: Result) {
        if (webVuw.canGoBack()) {
            webVuw.goBack()
        }
        result.success(null)
    }

    private fun goForward(methodCall: MethodCall, result: Result) {
        if (webVuw.canGoForward()) {
            webVuw.goForward()
        }
        result.success(null)
    }

    override fun onRefresh() {
        webVuw.reload()
        swipeRefresh?.isRefreshing = false
    }

    override fun dispose() {}

    override fun onListen(args: Any?, event: EventChannel.EventSink?) {
        eventSinkNavigation = event
    }

    override fun onCancel(p0: Any?) {
    }

    
    private fun getFileSize(fileUri: Uri): Long {
        val returnCursor = mContext?.getContentResolver()?.query(fileUri, null, null, null, null)
        returnCursor!!.moveToFirst()
        val sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE)
        return returnCursor.getLong(sizeIndex)
    }

    private fun getSelectedFiles(data: Intent): Array<Uri>? {
        // we have one files selected
        if (data.getData() != null) {
            val dataString = data.getDataString()
            if (dataString != null) {
                return arrayOf<Uri>(Uri.parse(dataString))
            }
        }
        // we have multiple files selected
        if (data.getClipData() != null) {
            val numSelectedFiles = data.getClipData()!!.getItemCount()
            var fileList: MutableList<Uri> = ArrayList()
            // val result = arrayOfNulls<Uri>(numSelectedFiles)
            for (i in 0 until numSelectedFiles) {
                fileList.add(data.getClipData()!!.getItemAt(i).getUri())
                // result[i] = data.getClipData()!!.getItemAt(i).getUri()
            }
            var result = fileList.toTypedArray()
            return result
        }
        return null
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?): Boolean {

        //super.onActivityResult(requestCode, resultCode, intent)

        Log.i("[WebView]", "onActivityResult: requestCode: $requestCode resultCode: $resultCode Activity.RESULT_OK ${Activity.RESULT_OK}")

        var handled: Boolean = false
        if (WBH5FaceVerifySDK.getInstance().receiveH5FaceVerifyResult(requestCode, resultCode, intent)) {
            return true
        }

        if (Build.VERSION.SDK_INT >= 21) {
            if (requestCode == FILECHOOSER_RESULTCODE) {

                Log.i("[WebView]","Activity.RESULT_OK ${Activity.RESULT_OK}")

                var results: Array<Uri>? = null
                if (resultCode == Activity.RESULT_OK) {

                    Log.i("[WebView]", "fileUri2 $fileUri ")

                    if (fileUri != null && getFileSize(fileUri!!) > 0) {
                        results = arrayOf<Uri>(fileUri!!)
                    } else if (videoUri != null && getFileSize(videoUri!!) > 0) {
                        results = arrayOf<Uri>(videoUri as Uri)
                    } else if (intent != null) {
                        results = getSelectedFiles(intent)
                    }
                }

                if (mUploadMessage5 != null) {
                    mUploadMessage5?.onReceiveValue(results)
                    mUploadMessage5 = null
                }

                handled = true
            }
            
        } else {
            if (requestCode == FILECHOOSER_RESULTCODE) {
                var result: Uri? = null
                if (resultCode == Activity.RESULT_OK && intent != null) {
                    result = intent.getData()
                }
                if (mUploadMessage != null) {
                    mUploadMessage?.onReceiveValue(result)
                    mUploadMessage = null
                }
                handled = true
            }
        }
        return handled
        
        /* 
        if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage) {
                handled = true
                return handled
            }
            val result = if (intent == null || resultCode != Activity.RESULT_OK)
                    null
                else
                    intent.getData()
            mUploadMessage?.onReceiveValue(result)
            mUploadMessage = null
            handled = true
        } else if (requestCode == FILECHOOSER_RESULTCODE_FOR_ANDROID_5) {
            if (null == mUploadMessage5) {
                handled = true
                return handled
            }
            mUploadMessage5?.onReceiveValue(WebChromeClient.FileChooserParams
                    .parseResult(resultCode, intent))
            mUploadMessage5 = null
            handled = true
        }
        return handled
        */
    }

    @TargetApi(7)
    inner class ResultHandler {
        public fun handelResult(requestCode: Int, resultCode: Int, intent: Intent?): Boolean {
            var handled: Boolean = false
            if (WBH5FaceVerifySDK.getInstance().receiveH5FaceVerifyResult(requestCode, resultCode, intent)) {
                return true
            }

            if (requestCode == FILECHOOSER_RESULTCODE) {
                if (null == mUploadMessage) {
                    handled = true
                    return handled
                }
                val result = if (intent == null || resultCode != Activity.RESULT_OK)
                        null
                    else
                        intent.getData()
                mUploadMessage?.onReceiveValue(result)
                mUploadMessage = null
                handled = true
            } else if (requestCode == FILECHOOSER_RESULTCODE_FOR_ANDROID_5) {
                if (null == mUploadMessage5) {
                    handled = true
                    return handled
                }
                mUploadMessage5?.onReceiveValue(WebChromeClient.FileChooserParams
                        .parseResult(resultCode, intent))
                mUploadMessage5 = null
                handled = true
            }
            return handled
        }
    }

    @JavascriptInterface
    fun showToast(json: String) {
        mContext?.let {
            mainActivity.runOnUiThread(java.lang.Runnable {
                it.toast(json)
            })
        }
    }

    @JavascriptInterface
    fun postMessage(json: String) {
        val result = HashMap<String, String>().apply {
            put(EVENT, json)
            put(URL, INJECT_URL)
        }

        mainActivity.runOnUiThread(java.lang.Runnable {
            eventSinkNavigation!!.success(result)
        })
    }

    // 当默认的onConsoleMessage不好使的时候使用的类
    class Console {
        private var TAG: String = "[WebView]"

        @JavascriptInterface
        fun log(msg: String) {
            Log.i(TAG, msg)
        }
        // 这里还可以添加其他方法console对象中有的方法,比如 assert

        @JavascriptInterface
        fun error(msg: String) {
            Log.e(TAG, msg)
        }

        @JavascriptInterface
        fun assert(msg: String) {
            Log.i(TAG, msg)
        }
    }
}

