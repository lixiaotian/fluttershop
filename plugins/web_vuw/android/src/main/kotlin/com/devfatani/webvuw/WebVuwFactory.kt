package com.devfatani.webvuw

import android.content.Context
import io.flutter.plugin.common.PluginRegistry.Registrar
import io.flutter.plugin.common.StandardMessageCodec
import io.flutter.plugin.platform.PlatformView
import io.flutter.plugin.platform.PlatformViewFactory

class WebVuwFactory private constructor(registrar: Registrar) : PlatformViewFactory(StandardMessageCodec.INSTANCE) {
    val TAG = "WebVuwFactory"
    private var register: Registrar = registrar
    private var webVuw: WebVuw? = null
    override fun create(context: Context, id: Int, args: Any): PlatformView {
        val params = args as Map<*, *>
        webVuw = WebVuw(register.activeContext(), register.messenger(), id, params, register.activity())
        register.addActivityResultListener(webVuw)
        return webVuw as WebVuw
    }

    fun getWebView(): WebVuw? {
        return webVuw
    }

    companion object {
        private var instance: WebVuwFactory? = null

        @Synchronized
        fun getInstance(registrar: Registrar?): WebVuwFactory {
            if (null == instance) {
                var node = WebVuwFactory(registrar as Registrar)
                instance = node
                return node
            }
            return instance as WebVuwFactory
        }
    }
}

