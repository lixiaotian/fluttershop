package com.devfatani.webvuw

import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.telephony.TelephonyManager
import android.util.Log
import android.webkit.ValueCallback
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView

import android.app.Activity.RESULT_OK

class WBH5FaceVerifySDK private constructor() {
    private var mUploadMessage: ValueCallback<Uri>? = null
    private var mUploadCallbackAboveL: ValueCallback<Array<Uri>>? = null

    fun setWebViewSettings(mWebView: WebView?, context: Context) {
        if (null == mWebView)
            return
        val webSetting = mWebView.settings
        webSetting.javaScriptEnabled = true
        webSetting.textZoom = 100
        webSetting.allowFileAccess = true
        webSetting.layoutAlgorithm = WebSettings.LayoutAlgorithm.NARROW_COLUMNS
        webSetting.setSupportZoom(true)
        webSetting.builtInZoomControls = true
        webSetting.useWideViewPort = true
        webSetting.setSupportMultipleWindows(false)
        webSetting.loadWithOverviewMode = true
        webSetting.setAppCacheEnabled(true)
        webSetting.databaseEnabled = true
        webSetting.domStorageEnabled = true
        webSetting.setGeolocationEnabled(true)
        webSetting.setAppCacheMaxSize(java.lang.Long.MAX_VALUE)
        webSetting.setAppCachePath(context.getDir("appcache", 0).path)
        webSetting.databasePath = context.getDir("databases", 0).path
        webSetting.setGeolocationDatabasePath(context.getDir("geolocation", 0).path)
        webSetting.pluginState = WebSettings.PluginState.ON_DEMAND
        webSetting.setRenderPriority(WebSettings.RenderPriority.HIGH)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            webSetting.allowUniversalAccessFromFileURLs = true
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mWebView.removeJavascriptInterface("searchBoxJavaBridge_")
        }
        val ua = webSetting.userAgentString
        try {
            webSetting.userAgentString = ua + ";webank/h5face;webank/1.0" + ";netType:" +
                    getNetWorkState(context) + ";appVersion:" +
                    context.packageManager.getPackageInfo(context.packageName, 0).versionCode +
                    ";packageName:" + context.packageName
        } catch (e: PackageManager.NameNotFoundException) {
            webSetting.userAgentString = "$ua;webank/h5face;webank/1.0"
            e.printStackTrace()
        }

    }

    fun receiveH5FaceVerifyResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        if (requestCode == VIDEO_REQUEST) { //根据请求码判断返回的是否是h5刷脸结果
            if (null == mUploadMessage && null == mUploadCallbackAboveL) {
                return true
            }
            val result = if (data == null || resultCode != RESULT_OK) null else data.data
            val uris = if (result == null) null else arrayOf(result)
            if (mUploadCallbackAboveL != null) {
                mUploadCallbackAboveL!!.onReceiveValue(uris)
                setmUploadCallbackAboveL(null)
            } else {
                mUploadMessage!!.onReceiveValue(result)
                setmUploadMessage(null)
            }
            return true
        }
        return false
    }

    fun recordVideoForApiBelow21(uploadMsg: ValueCallback<Uri>, acceptType: String, activity: Activity): Boolean {
        if ("video/webank" == acceptType) {
            setmUploadMessage(uploadMsg)
            recordVideo(activity)
            return true
        }
        return false
    }

    @TargetApi(21)
    fun recordVideoForApi21(webView: WebView, filePathCallback: ValueCallback<Array<Uri>>, activity: Activity, fileChooserParams: WebChromeClient.FileChooserParams): Boolean {
        Log.d("faceVerify", "accept is " + fileChooserParams.acceptTypes[0] + "---url---" + webView.url)
        if ("video/webank" == fileChooserParams.acceptTypes[0] || webView.url.startsWith("https://ida.webank.com/")) { //是h5刷脸
            setmUploadCallbackAboveL(filePathCallback)
            recordVideo(activity)
            return true
        }
        return false
    }

    /**
     * 调用系统前置摄像头进行视频录制
     */
    private fun recordVideo(activity: Activity) {
        try {
            val intent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            intent.putExtra("android.intent.extras.CAMERA_FACING", 1) // 调用前置摄像头
            activity.startActivityForResult(intent, VIDEO_REQUEST)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun setmUploadMessage(uploadMessage: ValueCallback<Uri>?) {
        mUploadMessage = uploadMessage
    }

    private fun setmUploadCallbackAboveL(uploadCallbackAboveL: ValueCallback<Array<Uri>>?) {
        mUploadCallbackAboveL = uploadCallbackAboveL
    }

    companion object {
        //没有网络连接
        private val NETWORK_NONE = "NETWORK_NONE"
        //wifi连接
        private val NETWORK_WIFI = "NETWORK_WIFI"
        //手机网络数据连接类型
        private val NETWORK_2G = "NETWORK_2G"
        private val NETWORK_3G = "NETWORK_3G"
        private val NETWORK_4G = "NETWORK_4G"
        private val NETWORK_MOBILE = "NETWORK_MOBILE"
        private val VIDEO_REQUEST = 0x11
        private var instance: WBH5FaceVerifySDK? = null

        /**
         * 获取当前网络连接类型
         *
         * @param context
         */
        private fun getNetWorkState(context: Context): String {
            //获取系统的网络服务
            val connManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                    ?: return NETWORK_NONE
            //如果当前没有网络

            //获取当前网络类型，如果为空，返回无网络
            val activeNetInfo = connManager.activeNetworkInfo
            if (activeNetInfo == null || !activeNetInfo.isAvailable) {
                return NETWORK_NONE
            }

            // 判断是不是连接的是不是wifi
            val wifiInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
            if (null != wifiInfo) {
                val state = wifiInfo.state
                if (null != state)
                    if (state == NetworkInfo.State.CONNECTED || state == NetworkInfo.State.CONNECTING) {
                        return NETWORK_WIFI
                    }
            }

            // 如果不是wifi，则判断当前连接的是运营商的哪种网络2g、3g、4g等
            val networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)

            if (null != networkInfo) {
                val state = networkInfo.state
                val strSubTypeName = networkInfo.subtypeName
                if (null != state)
                    if (state == NetworkInfo.State.CONNECTED || state == NetworkInfo.State.CONNECTING) {
                        when (activeNetInfo.subtype) {
                            //如果是2g类型
                            TelephonyManager.NETWORK_TYPE_GPRS // 联通2g
                                , TelephonyManager.NETWORK_TYPE_CDMA // 电信2g
                                , TelephonyManager.NETWORK_TYPE_EDGE // 移动2g
                                , TelephonyManager.NETWORK_TYPE_1xRTT, TelephonyManager.NETWORK_TYPE_IDEN -> return NETWORK_2G
                            //如果是3g类型
                            TelephonyManager.NETWORK_TYPE_EVDO_A // 电信3g
                                , TelephonyManager.NETWORK_TYPE_UMTS, TelephonyManager.NETWORK_TYPE_EVDO_0, TelephonyManager.NETWORK_TYPE_HSDPA, TelephonyManager.NETWORK_TYPE_HSUPA, TelephonyManager.NETWORK_TYPE_HSPA, TelephonyManager.NETWORK_TYPE_EVDO_B, TelephonyManager.NETWORK_TYPE_EHRPD, TelephonyManager.NETWORK_TYPE_HSPAP -> return NETWORK_3G
                            //如果是4g类型
                            TelephonyManager.NETWORK_TYPE_LTE -> return NETWORK_4G
                            else ->
                                //中国移动 联通 电信 三种3G制式
                                return if (strSubTypeName.equals("TD-SCDMA", ignoreCase = true) || strSubTypeName.equals("WCDMA", ignoreCase = true) || strSubTypeName.equals("CDMA2000", ignoreCase = true)) {
                                    NETWORK_3G
                                } else {
                                    NETWORK_MOBILE
                                }
                        }
                    }
            }
            return NETWORK_NONE
        }

        @Synchronized
        fun getInstance(): WBH5FaceVerifySDK {
            if (null == instance) {
                var node  = WBH5FaceVerifySDK()
                instance = node
                return node;
            }
            return instance as WBH5FaceVerifySDK;
        }
    }
}
