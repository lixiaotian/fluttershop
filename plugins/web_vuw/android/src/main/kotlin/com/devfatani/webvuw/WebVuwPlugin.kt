package com.devfatani.webvuw

import android.app.Activity
import android.content.Intent
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry
import io.flutter.plugin.common.PluginRegistry.Registrar
import android.util.Log

class WebVuwPlugin(val activity: Activity)  {
  var result: Result? = null
    companion object {
      private var webVuwFactory: WebVuwFactory? = null
      private var _registrar: Registrar? = null
        @JvmStatic
        fun registerWith(registrar: Registrar) {
            _registrar = registrar;
            webVuwFactory = WebVuwFactory.getInstance(registrar);
            registrar
                    .platformViewRegistry()
                    .registerViewFactory(
                            "plugins.devfatani.com/web_vuw", webVuwFactory)
        }

        fun getWebVuwFactory():WebVuwFactory? {
          if(webVuwFactory==null){
            webVuwFactory = WebVuwFactory.getInstance(_registrar);
          }
          return webVuwFactory; 
        }
    }

}

