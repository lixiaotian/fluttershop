#include "AppDelegate.h"
#include "GeneratedPluginRegistrant.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    //self.window.rootViewController = [[FlutterViewController alloc] init];
    //self.window.rootViewController.view.backgroundColor = [UIColor whiteColor];
    //self.window.backgroundColor = [UIColor whiteColor];
    //[self.window makeKeyAndVisible];
    
   [GeneratedPluginRegistrant registerWithRegistry:self];
   // Override point for customization after application launch.
    
   return [super application:application didFinishLaunchingWithOptions:launchOptions];
}


//- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
//  return [WXApi handleOpenURL:url delegate:[FluwxResponseHandler defaultManager]];
//}

// NOTE: 9.0以后使用新API接口
//- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options
//{
//  return [WXApi handleOpenURL:url delegate:[FluwxResponseHandler defaultManager]];
//}

@end


