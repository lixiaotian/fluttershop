import 'dart:convert';
import 'dart:io';
import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:my_app/home/HomePage.dart';
import 'package:my_app/home/IndexPage.dart';
import 'package:my_app/home/NewHomePage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:jpush_flutter/jpush_flutter.dart';

class Api {
  static final MODE = "release"; //debug  release

  static final APP_VERSION = "2.3.0";

  static final CONFIG_URL =
      "https://tata.baojiawangluo.com/static/config/tyconfig";

  static final PRIVATE_URL =
      "https://tata.baojiawangluo.com/nowle/protocal/private.html";

  static String SPLASH_URL = "";

  // 验证是否登录
  static bool Login_Bol = false;

  // Wechat APPID
  static final WECHAT_APPID = "wxe2e70598fd3befad";

  // apiUrl
  // static final API_URL = "http://192.168.32.65:8769";
  static String BASE_URL = MODE == "debug"
      ? "http://tatatest.baojiawangluo.com"
      : "https://tata.baojiawangluo.com";
      
  // apiUrl
  // static final API_URL = "http://192.168.32.65:8769";
  static String API_URL = MODE == "debug"
      ? "http://tytest.baojiawangluo.com"
      : "https://ty.baojiawangluo.com";

  // 支付URL
  static String API_ZhiFu_URL = MODE == "debug"
      ? "http://tytest.baojiawangluo.com/api"
      : "https://ty.baojiawangluo.com/api";

  // 图片URL
  static String API_IMG_URL = MODE == "debug"
      ? "http://tytest.baojiawangluo.com/ty/"
      : "http://shop.baojiawangluo.com/tuanyou/";

  // 图片URL
  static String ADV_URL = MODE == "debug"
      ? "http://tata.baojiawangluo.com/"
      : "https://tata.baojiawangluo.com/";

  // 图片URL
  static String UPDATE_URL = MODE == "debug"
      ? "http://tata.baojiawangluo.com/"
      : "http://tata.baojiawangluo.com/";

  //注册极光推送
  static JPush jpush = new JPush();

  static var SERVICE_PHONE = '110';

  // 设备ID
  static var DEVICEID = "";

  // 收款银行信息
  static var BANK_INFO = "收款人:北京麻利科技有限公司, 账号:000000000000";

  // 配置数据
  static var CONFIGDATA;

  static String logintoken = "";

  // 邀请链接
  static getInviteUrl() {
    if (MODE == "debug") {
      return "http://tytest.baojiawangluo.com/h5/nowle/protocal/?token=" +
          logintoken +
          "#/";
    } else {
      return "https://t.baojiawangluo.com/nowle/protocal/?token=" +
          logintoken +
          "#/";
    }
  }



  // 获取设备ID
  static Future<String> _getDeviceId() async {
    WidgetsFlutterBinding.ensureInitialized();

    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }

  //注册极光推送
  static Future<dynamic> addTags(var username) async {
    if (username != null && username != "") {
      jpush.addTags(["tuan_user" + username]);
    }
    return new Future(() => true);
  }
}

List<T> worker<T>(List list, Function handler) {
  List<T> result = [];
  for (var i = 0; i < list.length; i++) {
    result.add(handler(i, list[i]));
  }
  return result;
}

dynamic parseEventData(data) {
  try {
    var event = json.decode(data);
    print("${event}");
    return event;
  } catch (ex) {
    print(ex.toString());
    return null;
  }
}

Home GetHomePage({int index: 0}) {
  return new NewHome(id: index);
}