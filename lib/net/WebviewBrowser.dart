import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:my_app/first/widgetFit.dart';
import 'package:my_app/net/Api.dart';
import 'package:web_vuw/web_vuw.dart';
import 'package:fluwx/fluwx.dart' as fluwx;
import 'package:my_app/first/toast.dart' as prefix1;
//import 'webview.dart';

// ignore: must_be_immutable
class WebviewBrowser extends StatefulWidget {
  String url, title;
  // CustomerBrowser ({this._url = _url;this._title = _title; });
  WebviewBrowser({this.url, this.title});
  @override
  _WebviewBrowser createState() => new _WebviewBrowser();
}

class _WebviewBrowser extends State<WebviewBrowser> {
  bool _isLoading = false;
  final Completer<WebVuwController> _controller = Completer<WebVuwController>();
  StreamSubscription _ssWebVuwEvents;
  var _loadUrl = '';
  var _title = '';
  @override
  initState() {
    var title = widget.title;
    var url = widget.url;
    print(url);
    setState(() {
      _loadUrl = url;
      _title = title;
    });

    super.initState();
  }

  BackPage(msg) {
    if (msg != '' && msg != null) {
      prefix1.Toast.show(context, msg);
      Navigator.pop(context);
      prefix1.Toast.show(context, msg);
    } else {
      Navigator.pop(context);
    }
  }


  @override
  Widget build(BuildContext context) {
    return FutureBuilder<WebVuwController>(
        future: _controller.future,
        builder:
            (BuildContext context, AsyncSnapshot<WebVuwController> snapshot) {
          final webViewReady = snapshot.connectionState == ConnectionState.done;
          final controller = snapshot.data;

          if (webViewReady) {
            _ssWebVuwEvents = controller.onEvents().listen((events) {
              print('Current Events 😎=> $events');
              var event = events["event"];
              if (event.startsWith("{")) {
                if (event == "didFinish" || event == "onPageFinished") {
                  if (mounted) {
                    setState(() {
                      _isLoading = false;
                    });
                  }
                } else if (event == "didStart" || event == "onPageStarted") {
                  if (mounted) {
                    setState(() {
                      _isLoading = true;
                    });
                    print(_isLoading);
                  }
                } else {
                  print(parseEventData(event));
                  var evet = parseEventData(event);
                  print(evet['name']);
                  if (evet['name'] == 'back') {
                    BackPage(evet['msg']);
                  }
                  
                  /**处理自定义消息**/
                }
              } else {
                print(123);
                print(event.runtimeType);
              }
            });
          }
          return Scaffold(
            appBar: AppBar(
                centerTitle: true,
                elevation: 0.5,
                title: Text(
                  _title,
                  style: TextStyle(fontSize: widgetFit.unit * 34),
                ),
                actions: <Widget>[]),
            body: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: WebVuw(
              initialUrl: _loadUrl,
              enableJavascript: true,
              pullToRefresh: false,
              // userAgent: 'iphone',
              // userAgent: 'userAgent',
              gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
                Factory<OneSequenceGestureRecognizer>(
                  () => EagerGestureRecognizer(),
                ),
              ].toSet(),
              javaScriptMode: JavaScriptMode.unrestricted,
              onWebViewCreated: (WebVuwController webViewController) {
                _controller.complete(webViewController);
              },
            )),
          );
        });
  }
}
