import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:my_app/first/widgetFit.dart';
import 'package:my_app/net/Api.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:my_app/first/toast.dart' as prefix1;

class TsUtils {
  static Future clearShared() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  Future saveString(property, message) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(property, message);
  }

  static Future saveBoolean(property, message) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setBool(property, message);
  }

  static Future<dynamic> showLH(BuildContext _context) {
    return showDialog<void>(
        context: _context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            title: Text(
              '账号异常，请联系客服',
              style: TextStyle(
                  color: Color(0XFF666666), fontSize: widgetFit.unit * 30),
            ),
            actions: <Widget>[
              new CupertinoButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    '确认',
                    style: TextStyle(
                        color: Color(0XFFFF8527),
                        fontSize: widgetFit.unit * 30),
                  )),
            ],
          );
        });
  }

  // showloading
  static Future<dynamic> showloading(BuildContext _context, String text) {
    return showDialog<void>(
        context: _context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new Material(
            type: MaterialType.transparency,
            child: new Center(
              child: new SizedBox(
                width: 120.0,
                height: 120.0,
                child: new Container(
                  decoration: ShapeDecoration(
                    color: Color(0xffffffff),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(8.0),
                      ),
                    ),
                  ),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new CircularProgressIndicator(),
                      new Padding(
                        padding: const EdgeInsets.only(
                          top: 20.0,
                        ),
                        child: new Text(text),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  // showShort
  static showShort(String msg) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        fontSize: 16,
        backgroundColor: Color(0xFF373C41),
        textColor: Color(0xFFFFFFFF));
  }

  // showToast
  static showToast(String msg) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        fontSize: 16,
        backgroundColor: Color(0xFF373C41),
        textColor: Color(0xFFFFFFFF));
  }

  // 显示对话框
  static Future<dynamic> showModelDialog(
      BuildContext _context, String _title, Widget _widget) {
    return showDialog<Null>(
      context: _context,
      builder: (BuildContext context) {
        return new SimpleDialog(
          title: new Text(_title),
          children: <Widget>[_widget],
        );
      },
    );
  }

  // 显示对话框
  static Future<dynamic> showAdvetDialog(
      BuildContext _context, Widget _widget) {
    return showDialog<Null>(
      context: _context,
      builder: (BuildContext context) {
        return new SimpleDialog(
          backgroundColor: Color(0x00FFFFFF),
          children: <Widget>[_widget],
        );
      },
    );
  }

  
}
