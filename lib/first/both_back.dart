import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:my_app/first/iconfont.dart';
import '../first/widgetFit.dart';

class Both{
  static back(BuildContext context) {
      return GestureDetector(
              child: Container(
              color: Color(0x00666666),
              padding: EdgeInsets.fromLTRB(widgetFit.unit * 0, 0, 0, 0),
              child: Icon(MyIconFont.back,size: widgetFit.unit * 44,color: Color(0xFF666666),)
              ),
              onTap: ()=>{
              Navigator.pop(context)
              },
              );
    }
}