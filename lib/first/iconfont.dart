import 'package:flutter/widgets.dart';


// 代码由程序自动生成。
// 请不要对此文件做任何修改。详细说明见本文件末尾


class MyIconFont {

    MyIconFont._();

    static const font_name = 'MyIconFont';

    static const IconData renzheng = const IconData(0xe604, fontFamily: font_name);
    static const IconData shenghuoyongping = const IconData(0xe618, fontFamily: font_name);
    static const IconData shell = const IconData(0xe605, fontFamily: font_name);
    static const IconData loginout = const IconData(0xe614, fontFamily: font_name);
    static const IconData go = const IconData(0xe612, fontFamily: font_name);
    static const IconData go_bottom = const IconData(0xe624, fontFamily: font_name);
    static const IconData dianhua = const IconData(0xe620, fontFamily: font_name);
    static const IconData shezhi = const IconData(0xe621, fontFamily: font_name);
    static const IconData jia = const IconData(0xe608, fontFamily: font_name);
    static const IconData back = const IconData(0xe654, fontFamily: font_name);
    static const IconData vip = const IconData(0xe607, fontFamily: font_name);
    static const IconData liulan = const IconData(0xe60B, fontFamily: font_name);
    static const IconData pinglun = const IconData(0xe609, fontFamily: font_name);
    static const IconData friend_pinglun = const IconData(0xe61a, fontFamily: font_name);
    static const IconData like_error = const IconData(0xe61b, fontFamily: font_name);
    static const IconData like_success = const IconData(0xe629, fontFamily: font_name);
    static const IconData arrows_right = const IconData(0xe623, fontFamily: font_name);
    static const IconData arrows_bottom = const IconData(0xe61e, fontFamily: font_name);
    static const IconData arrows_top = const IconData(0xe61f, fontFamily: font_name);
    static const IconData fenxiang = const IconData(0xe60a, fontFamily: font_name);
    static const IconData tuandantoutiao = const IconData(0xe622, fontFamily: font_name);
    static const IconData shanchu = const IconData(0xe619, fontFamily: font_name);
    static const IconData choose_one = const IconData(0xe60c, fontFamily: font_name);
    static const IconData choose_two = const IconData(0xe60d, fontFamily: font_name);
    static const IconData choose_three = const IconData(0xe60e, fontFamily: font_name);
    static const IconData photo = const IconData(0xe625, fontFamily: font_name);
    static const IconData search = const IconData(0xe626, fontFamily: font_name);
    static const IconData empty = const IconData(0xe628, fontFamily: font_name);
    static const IconData checked = const IconData(0xe602, fontFamily: font_name);
    static const IconData close = const IconData(0xe60f, fontFamily: font_name);   
/*、
如果你有新的图标需要导入到字体中：

1. 联系我，将你加入到 iconfont(http://www.iconfont.cn) 的 xxx 项目中
2. 在 iconfont 中选择需要的图标，加入购物车
3. 然后点击顶部的购物车，弹出右边栏，选择“添加到项目”，选择 xxx，点击“确定”
4. 最好修改一下每个新增图标的名称，然后下载至本地
5. 将压缩包内所有文件拷贝到 iconfont 目录下，形如：
iconfont
├── iconfont.css
├── iconfont.eot
├── iconfont.js
├── iconfont.svg
├── iconfont.ttf
└── iconfont.woff】】1

6. 拷贝完成后通过以下命令完成转换：
   $ python3 ./translate_icon_font_from_css_to_dart.py
7. 其中转换时需要 iconfont.ttf 和 iconfont.css 文件;实际项目运行时只需要 iconfont.ttf 文件。其余文件不需要。
8. 开发时，通过右键 _fontclass.html 使用 Chrome 打开来查看图标对应的 unicode 码，使用时，将中划线“-”替换为下划线“_”。
*/
}