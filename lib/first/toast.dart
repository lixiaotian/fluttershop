import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ToastView {
  OverlayEntry overlayEntry;
  OverlayState overlayState;
  bool dismissed = false;

  _show() async {
    overlayState.insert(overlayEntry);
    await Future.delayed(Duration(milliseconds: 1500));
    this.dismiss();
  }

  dismiss() async {
    if (dismissed) {
      return;
    }
    this.dismissed = true;
    overlayEntry?.remove();
  }
}

class Toast {
  
  static ToastView preToast;
  static show(BuildContext context, String msg) {
    preToast?.dismiss();
    preToast = null;
    var overlayState = Overlay.of(context);
    OverlayEntry overlayEntry;
    overlayEntry = new OverlayEntry(builder: (context) {
      return buildToastLayout(msg);
    });
    var toastView = ToastView();
    toastView.overlayState = overlayState;
    toastView.overlayEntry = overlayEntry;
    preToast = toastView;
    toastView._show();
  }
  static LayoutBuilder buildToastLayout(String msg) {
    return LayoutBuilder(builder: (context, constraints) {
      return IgnorePointer(
        ignoring: true,
        child: Container(
          child: Material(
            color: Colors.white.withOpacity(0),
            child: Container(
              child: Container(
                child: Text(
                  "${msg}",
                  style: TextStyle(color: Colors.white),
                ),
                decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.6),
                  borderRadius: BorderRadius.all(
                    Radius.circular(5),
                  ),
                ),
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              ),
              margin: EdgeInsets.only(
                top: constraints.biggest.height * 0.15,
                bottom: constraints.biggest.height * 0.15,
                left: constraints.biggest.width * 0.2,
                right: constraints.biggest.width * 0.2,
              ),
            ),
          ),
          alignment: Alignment.center,
        ),
      );
    });
  }
}

// //获取OverlayState
// OverlayState overlayState = Overlay.of(context);
// //创建OverlayEntry
// OverlayEntry _overlayEntry = OverlayEntry(builder:(BuildContext context) => Positioned(child:Icon(Icons.check_circle),));
// //显示到屏幕上
// overlayState.insert(_overlayEntry);
// //移除屏幕
// overlayState.remove();

// //创建OverlayEntry
// Overlay entry=new OverlayEntry(builder:(){/*在这里创建对应的widget*/});
// //往Overlay中插入插入OverlayEntry
// Overlay.of(context).insert(overlayEntry);

// class Toast {
//   static void show({@required BuildContext context, @required String message}) {
//     //创建一个OverlayEntry对象
//     OverlayEntry overlayEntry = new OverlayEntry(builder: (context) {
//     //外层使用Positioned进行定位，控制在Overlay中的位置
//       return new Positioned(
//           top: MediaQuery.of(context).size.height * 0.7,
//           child: new Material(
//             child: new Container(
//               width: MediaQuery.of(context).size.width,
//               alignment: Alignment.center,
//               child: new Center(
//                 child: new Card(
//                   child: new Padding(
//                     padding: EdgeInsets.all(8),
//                     child: new Text(message),
//                   ),
//                   color: Colors.grey,
//                 ),
//               ),
//             ),
//           ));
//     });
//     //往Overlay中插入插入OverlayEntry
//     Overlay.of(context).insert(overlayEntry);
//     //两秒后，移除Toast
//     new Future.delayed(Duration(seconds: 2)).then((value) {
//       overlayEntry.remove();
//     });
//   }
// }
