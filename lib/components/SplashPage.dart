import 'package:flutter/material.dart';
import 'package:my_app/home/HomePage.dart';
import 'dart:async';
import 'package:my_app/net/Api.dart';
import 'package:my_app/net/TsUtils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashState createState() => new _SplashState();
}

class _SplashState extends State<SplashPage> {
  var first_bol = false;
  Future getString() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var first_time = sharedPreferences.getBool('first_time');
    print(first_time);
    if (first_time == true) {
      setState(() {
        first_bol = true;
      });
    } else {
      TsUtils.saveBoolean('first_time', false);
      setState(() {
        first_bol = false;
      });
    }
  }

  @override
  void initState() {
    getString();

    new Future.delayed(Duration(seconds: 5), () {
      print("麻利商城启动...");
      
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    if (Api.SPLASH_URL != "") {
      return new Container(
        color: Color(0xFFFFFFFF),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Image.network(Api.SPLASH_URL,
            width: double.infinity, height: double.infinity, fit: BoxFit.cover),
      );
    } else {
      return new Container(
        color: Color(0xFFFFFFFF),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Image.asset("assets/image/splash.png",
            width: double.infinity, height: double.infinity, fit: BoxFit.cover),
      );
    }
  }
}
