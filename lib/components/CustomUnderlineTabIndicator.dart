// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import '../first/widgetFit.dart';
//import 'colors.dart';

/// Used with [TabBar.indicator] to draw a horizontal line below the
/// selected tab.
///
/// The selected tab underline is inset from the tab's boundary by [insets].
/// The [borderSide] defines the line's color and weight.
///
/// The [TabBar.indicatorSize] property can be used to define the indicator's
/// bounds in terms of its (centered) widget with [TabIndicatorSize.label],
/// or the entire tab with [TabIndicatorSize.tab].
class CustomUnderlineTabIndicator extends Decoration {
  /// Create an underline style selected tab indicator.
  ///
  /// The [borderSide] and [insets] arguments must not be null.
  const CustomUnderlineTabIndicator({
    this.borderSide = const BorderSide(width: 2.0, color: Colors.white),
    this.insets = EdgeInsets.zero,
  })  : assert(borderSide != null),
        assert(insets != null);

  /// The color and weight of the horizontal line drawn below the selected tab.
  final BorderSide borderSide;

  /// Locates the selected tab's underline relative to the tab's boundary.
  ///
  /// The [TabBar.indicatorSize] property can be used to define the
  /// tab indicator's bounds in terms of its (centered) tab widget with
  /// [TabIndicatorSize.label], or the entire tab with [TabIndicatorSize.tab].
  final EdgeInsetsGeometry insets;

  @override
  Decoration lerpFrom(Decoration a, double t) {
    if (a is CustomUnderlineTabIndicator) {
      return CustomUnderlineTabIndicator(
        borderSide: BorderSide.lerp(a.borderSide, borderSide, t),
        insets: EdgeInsetsGeometry.lerp(a.insets, insets, t),
      );
    }
    return super.lerpFrom(a, t);
  }

  @override
  Decoration lerpTo(Decoration b, double t) {
    if (b is CustomUnderlineTabIndicator) {
      return CustomUnderlineTabIndicator(
        borderSide: BorderSide.lerp(borderSide, b.borderSide, t),
        insets: EdgeInsetsGeometry.lerp(insets, b.insets, t),
      );
    }
    return super.lerpTo(b, t);
  }

  @override
  _UnderlinePainter createBoxPainter([VoidCallback onChanged]) {
    return _UnderlinePainter(this, onChanged);
  }
}

class _UnderlinePainter extends BoxPainter {
  _UnderlinePainter(this.decoration, VoidCallback onChanged)
      : assert(decoration != null),
        super(onChanged);

  final CustomUnderlineTabIndicator decoration;

  BorderSide get borderSide => decoration.borderSide;
  EdgeInsetsGeometry get insets => decoration.insets;

  Rect _indicatorRectFor(Rect rect, TextDirection textDirection) {
    assert(rect != null);
    assert(textDirection != null);
    final Rect indicator = insets.resolve(textDirection).deflateRect(rect);
    return Rect.fromLTWH(
      indicator.left,
      indicator.bottom - borderSide.width,
      indicator.width,
      borderSide.width,
    );
  }

  double convertRadiusToSigma(double radius) {
    return radius * 0.57735 + 0.5;
  }

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration configuration) {
    assert(configuration != null);
    assert(configuration.size != null);
    final Rect rect = offset & configuration.size;
    final TextDirection textDirection = configuration.textDirection;
    final Rect _indicator =
        _indicatorRectFor(rect, textDirection).deflate(borderSide.width / 2.0);
    Rect indicator;

    indicator = Rect.fromLTWH(_indicator.left + (_indicator.width - 24) / 2,
        _indicator.top + 2, 24, _indicator.height);

    canvas.drawShadow(
        Path()
          ..moveTo(indicator.left, indicator.top)
          ..lineTo(indicator.right, indicator.top)
          ..lineTo(indicator.right, indicator.bottom)
          ..lineTo(indicator.left, indicator.bottom)
          ..close(),
        Color(0xFFFF8527),
        6,
        true);

    //final Paint paint = borderSide.toPaint()..strokeCap = StrokeCap.square;
    final Paint paint = borderSide.toPaint()
      ..strokeCap = StrokeCap.round
      ..isAntiAlias = true
      ..strokeWidth = 6.0
      ..style = PaintingStyle.stroke;

    canvas.drawPath(
        Path()
          ..addRect(Rect.fromPoints(
              Offset(indicator.left - 5, indicator.top + 1),
              Offset(indicator.right + 5, indicator.bottom + 5)))
          ..addOval(Rect.fromPoints(Offset(indicator.left, indicator.top),
              Offset(indicator.right, indicator.bottom)))
          ..fillType = PathFillType.evenOdd,
        Paint()
          ..color = Color(0x66FF8527)
          ..maskFilter =
              MaskFilter.blur(BlurStyle.normal, convertRadiusToSigma(3)));

    canvas.drawLine(indicator.bottomLeft, indicator.bottomRight, paint);
    //canvas.drawLine(indicator.bottomLeft, indicator.bottomRight, paint);
  }
}
