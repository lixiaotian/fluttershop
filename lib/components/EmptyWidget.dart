// ignore: must_be_immutable
import 'package:flutter/material.dart';
import '../first/widgetFit.dart';

class EmptyWidget extends StatelessWidget {
  String msg = "";
  EmptyWidget(this.msg);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height - 168,
      child: Column(
        children: <Widget>[
          Container(
            width: widgetFit.unit * 520,
            height: widgetFit.unit * 208,
            margin: EdgeInsets.fromLTRB(0, widgetFit.unit * 350, 0, 0),
            child: Image.asset('assets/image/service_two/NoGoodList.png',
                fit: BoxFit.fill),
          ),
          Container(
            child: Text(this.msg,
                style: TextStyle(
                    color: Color(0xFFAEB0BD), fontSize: widgetFit.unit * 30)),
            margin: EdgeInsets.fromLTRB(0, widgetFit.unit * 94, 0, 0),
          )
        ],
      ),
    );
  }
}
