class FunctionModel {
  var id;
  String title;
  String type;
  String url;
  String icon;
  int requireLogin;

  FunctionModel(this.id, this.title, this.type, this.icon,
      {String url, int requireLogin: 0}) {
    this.url = url;
    this.requireLogin = requireLogin;
  }

  FunctionModel.fromJson(jsonRes) {
    this.id = jsonRes["id"];
    this.title = jsonRes["title"];
    this.type = jsonRes["type"];
    this.url = jsonRes["url"];
    this.icon = jsonRes["icon"];
    this.requireLogin = jsonRes["requireLogin"];
  }

  @override
  String toString() {
    return '{"id": $id,"title": "$title","type": "$type","url": "$url","icon": "$icon","requireLogin": "$requireLogin"}';
  }
}
