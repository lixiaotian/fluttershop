class Advert {
  var id;
  String title;
  String image;
  String banner;
  String url;
  Function callback;
  Advert(
      {this.id, this.title, this.image, this.banner, this.url, this.callback});
}
