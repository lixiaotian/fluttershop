// ignore: must_be_immutable
import 'package:flutter/material.dart';
import '../first/widgetFit.dart';

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      Positioned(
        top: 0,
        left: 0,
        child: Offstage(
            offstage: false,
            child: Container(
              color: Color(0xFFFFFFFF),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              alignment: Alignment.center,
              child: CircularProgressIndicator(
                backgroundColor: Color(0xFFFF8F8F8),
                valueColor:
                    new AlwaysStoppedAnimation<Color>(Color(0xFFFFDC28)),
              ),
            )),
      )
    ]);
  }
}
