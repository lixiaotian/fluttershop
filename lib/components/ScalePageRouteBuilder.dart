import 'package:flutter/material.dart';

/// 自定义页面切换动画 - 缩放切换
class ScalePageRouteBuilder extends PageRouteBuilder{
  // 跳转的页面
  final Widget widget;
  ScalePageRouteBuilder(this.widget):super(
      transitionDuration:Duration(seconds: 1),
      pageBuilder:(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation){
        return widget;
      },
      transitionsBuilder:(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child){
        return ScaleTransition(child:child,
            scale: Tween(begin: 0.0,end: 1.0)
                .animate(CurvedAnimation(parent: animation, curve: Curves.fastOutSlowIn))
        );
      }
  );
}
