import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:my_app/net/TsUtils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './net/Api.dart';
import 'package:fluwx/fluwx.dart' as fluwx;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await fluwx.registerWxApi(
      appId: "wx76107a1f73e8e9ad",
      doOnAndroid: true,
      doOnIOS: true,
      universalLink: "https://tata.baojiawangluo.com/nowle/");

  fluwx.isWeChatInstalled().then((result) {
    print("WeChat is installed $result");
    if (result) {
      fluwx.responseFromShare.listen((response) {}); //分享
      fluwx.responseFromAuth.listen((response) {}); //授权
      fluwx.responseFromPayment.listen((data) {
        print("${data.errCode}");
      }); //支付
    }
  }).catchError((e) {
    print(e);
  });


  return runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MyApp();
}

class _MyApp extends State<MyApp> {
  var first_bol = false;
  @override
  void initState() {
    
    super.initState();
  }


  Widget HomePage() {
    // if (Api.SPLASH_URL != "") {
    //   return SplashPage();
    // } else {
    //   return first_bol ? GetHomePage() : GuidePage();
    // }
    return GetHomePage();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      color: Colors.white,
      debugShowCheckedModeBanner: false, //顶部debug条幅隐藏
      home: HomePage(), // first_bol ? Home() : GuidePage(),
      theme: ThemeData(
          primaryColor: const Color(0xFFFFFFFF),
          primaryColorBrightness: Brightness.light,
          highlightColor: Color.fromRGBO(255, 255, 255, 1),
          backgroundColor: Color(0xFFEEEEEE),
          bottomAppBarColor: Color(0xFFFFFFFF),
          splashColor: Colors.white70, //水波纹颜色
          scaffoldBackgroundColor: Color(0xFFFFFFFF)),
    );
  }
}

// class MyApp extends StatelessWidget {
//   Future getString() async {
//     SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
//     TsUtils.saveBoolean('first_time',false);
//     var first_time = sharedPreferences.getBool('login');
//     if (first_time == true) {

//     }else{

//     }
//   }
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false, //顶部debug条幅隐藏
//       home: Home(),
//       theme: ThemeData(
//           //主题颜色
//           primaryColorBrightness: Brightness.light,
//           highlightColor: Color.fromRGBO(255, 255, 255, 0.5),
//           splashColor: Colors.white70, //水波纹颜色
//           primaryColor: Colors.white),
//     );
//   }
// }
