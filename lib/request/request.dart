
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:my_app/request/Http.dart';

class Request {
  Dio dio = new Dio();
  get(url) async {
    return Http.get(url);
  }

  post(url, {data = null}) async {
    Response response;
    try {
      response = await dio.post(
        url,
        data: data != null ? data : {},
        // cancelToken:cancelToken
      );
    } on DioError catch (e) {
      if (CancelToken.isCancel(e)) {
        print('post请求取消! ' + e.message);
      } else {
        print('post请求发生错误：$e');
      }
      return null;
    }
    return response.data;
  }

  getjson(url) async {
    Response response;
    try {
      response = await dio.get(url);
    } catch (e) {
      print(e);
    }
    return response.data;
  }
  
}
