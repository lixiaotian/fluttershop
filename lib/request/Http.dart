import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

/*
 * http请求
 */
class Http {
  static const SP_COOKIE = 'sp_cookie';
  //  get 请求
  static Future<dynamic> request(String url,
      {Map<String, String> params, bool saveCookie = false}) async {
    if (params == null) {
      params = new Map();
    }
    String _url = url;
    if (params != null && params.isNotEmpty) {
      StringBuffer sb = new StringBuffer("?");
      params.forEach((key, value) {
        sb.write("$key" + "=$value" + "&");
      });
      String paramStr = sb.toString();
      log('参数是$params');
      paramStr = paramStr.substring(0, paramStr.length - 1);
      _url += paramStr;
    }
    // log('url是$_url');
    var headers = await _getHeader();
    http.Response res = await http.get(_url, headers: headers);
    if (res.statusCode == 200) {
      var cookie = res.headers['set-cookie'];
      if (saveCookie) {
        saveCookies(cookie);
      }
      String body = res.body;
      log(body);
      var jsonStr = json.decode(body);
      return jsonStr;
    } else {
      return error("您的网络好像不太好哟");
    }
  }

  //  get 请求
  static Future<dynamic> get(String url,
      {Map<String, String> params, bool saveCookie = false}) async {
    if (params == null) {
      params = new Map();
    }
    String _url = url;
    if (params != null && params.isNotEmpty) {
      StringBuffer sb = new StringBuffer("?");
      params.forEach((key, value) {
        sb.write("$key" + "=$value" + "&");
      });
      String paramStr = sb.toString();
      log('参数是$params');
      paramStr = paramStr.substring(0, paramStr.length - 1);
      _url += paramStr;
    }
    //log('url是$_url');
    var headers = await _getHeader();
    http.Response res = await http.get(_url, headers: headers);

    if (res.statusCode == 200) {
      var cookie = res.headers['set-cookie'];
      if (saveCookie) {
        saveCookies(cookie);
      }
      String body = res.body;
      log("请求返回结果${url}");
      log(body);
      try {
        var jsonStr = json.decode(body);
        int errCode = jsonStr['errorCode'] ?? jsonStr['error'];
        if (errCode == 0) {
          dynamic data = jsonStr['data'];
          log('the data of method is $data');
          return jsonStr;
        } else {
          print(jsonStr['msg']);
          return jsonStr;
        }
      } catch (e) {
        print(e);
        return error("错误的数据格式");
      }
    } else {
      print('您的网络好像不太好哟~');
      return error("您的网络好像不太好哟");
    }
  }

  //  get 请求
  static Future<dynamic> getApi(String url,
      {Map<String, dynamic> params, bool saveCookie = false}) async {
    if (params == null) {
      params = new Map();
    }

    String _url = url;
    if (params != null && params.isNotEmpty) {
      StringBuffer sb = new StringBuffer("?");
      params.forEach((key, value) {
        sb.write("$key" + "=$value" + "&");
      });
      String paramStr = sb.toString();
      paramStr = paramStr.substring(0, paramStr.length - 1);
      _url += paramStr;
    }
    print(' ------------- url是:$_url,参数是:$params');
    var headers = await _getHeader();
    http.Response res = await http.get(_url, headers: headers);
    if (res.statusCode == 200) {
      var cookie = res.headers['set-cookie'];
      if (saveCookie) {
        saveCookies(cookie);
      }
      String body = res.body;
      //log('url是:$_url,返回数据:${res.body}');
      try {
        var jsonStr = json.decode(body);
        int errCode = jsonStr['errorCode'] ?? jsonStr['error'];
        if (errCode == 0) {
          return jsonStr;
        } else {
          return jsonStr;
        }
      } catch (e) {
        log(e.toString());
        print("系统错误");
        return error("错误的数据格式");
      }
    } else {
      log('网络异常，url是:$_url,返回数据:${res.body}');
      return error("您的网络好像不太好哟");
    }
  }

//  post请求
  static Future<Map> post(String url,
      {Map<String, String> params, bool saveCookie = false}) async {
    if (params == null) {
      params = new Map();
    }
    String _url = url;
    var cookie = await getCookies();
    if (cookie != null) {
      params['Cookie'] = cookie;
    }
    var headers = await _getHeader();
    http.Response res = await http.post(_url, headers: headers, body: params);
    return _dealWithRes(res, saveCookie: saveCookie);
  }

//  post请求
  static Future<Map> postApi(String url,
      {Map<String, dynamic> params, bool saveCookie = false}) async {
    if (params == null) {
      params = new Map();
    }
    String _url = url;
    var cookie = await getCookies();
    if (cookie != null) {
      params['Cookie'] = cookie;
    }
    var headers = await _getHeader();
    var data = json.encode(params);
    headers["Content-Type"] = "application/json";
    http.Response res = await http.post(_url, headers: headers, body: data);
    return _dealWithRes(res, saveCookie: saveCookie);
  }

// 处理 post 数据
  static Map<String, dynamic> _dealWithRes(var res, {bool saveCookie}) {
    if (res.statusCode == 200) {
      var cookie = res.headers['set-cookie'];
      if (saveCookie) {
        saveCookies(cookie);
      }
      String body = res.body;
      try {
        var jsonStr = json.decode(body);
        int errCode = jsonStr['errorCode'] ?? jsonStr['error'];
        if (errCode == 0) {
          dynamic data = jsonStr['data'];
          log('the data of method is $data');
          return jsonStr;
        } else {
          print(jsonStr['msg']);
          return jsonStr;
        }
      } catch (e) {
        print(e);
        return error("错误的数据格式");
      }
    } else {
      return error("您的网络好像不太好哟");
    }
  }

  // 添加Header信息
  static Future<Map<String, String>> _getHeader() async {
    Map<String, String> headers = new Map();
    return new Future(() => headers);
  }

  static void log(obj) {
    print(obj);
  }

  static void saveCookies(var cookie) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(SP_COOKIE, cookie);
  }

  static Future<String> getCookies() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var cookieStr = sharedPreferences.getString(SP_COOKIE);
    return cookieStr;
  }

  static Map success({String msg: "OK"}) {
    return {error: 0, msg: msg};
  }

  static Map error(String msg) {
    return {error: 500, msg: msg};
  }
}
