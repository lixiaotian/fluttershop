import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_refresh/flutter_refresh.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:my_app/net/Api.dart';
import 'package:my_app/net/WebviewBrowser.dart';
import 'package:my_app/net/compatibleHttp.dart';
import 'package:my_app/first/both_back.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:my_app/first/toast.dart' as prefix1;
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:my_app/components/CustomTabBar.dart';
import '../first/widgetFit.dart';
import '../request/request.dart';
import '../first/iconfont.dart';
import 'package:my_app/first/toast.dart' as prefix1;
class IndexPage extends StatefulWidget {
  @override
  IndexPageState createState() {
    return IndexPageState();
  }
}

class IndexPageState extends State<IndexPage> with TickerProviderStateMixin {
  TabController Tabcontroller;
  EasyRefreshController _controller;
  final ScrollController _scrollController = new ScrollController();
  final ScrollController _mScrollController = new ScrollController();
  var GoodsTypeIndex = 0;
  var GoodsTypeList = [];
  var RecommendGoodsList = [];
  var GoodsList = [];
  var GoodsListSize = 0;
  var GoodsListPage = 1;
  var AdsList = [];
  var UserIdentificationBool = false;
  var AssetAlterPositionBol = true;
  var HotShopList = [];
  var HotMerchantList = [];
  var TextSwiperList = [];
  var ImgSwiperList = [];
  var HotShopListSize = 0;
  var HotMerchantListSize = 0;
  var shell_true = false;
  var shellcolor = Colors.white;
  var hot_merchant = false;
  var DealRentGoodsBool = false;
  var hot_shop = true;
  var hot_merchantJc = 'w700';
  var hot_shopJc = 300;
  var hot_merchantFz = widgetFit.unit * 36.0;
  var hot_shopFz = widgetFit.unit * 32.0;
  var selTab = 0;
  var opacityTop = 0.0;
  var opacityTitle = 0.0;
  var _token = '';
  var _hotMershantPage = 1;
  var _gethotIndexPage = 1;
  bool initAdv = false;
  bool clickAdv = false;
  bool isPerformingRequest = false;

  bool isOpenEyeBool = false;// 是否开启 租金显示
  bool isAlBuyBool = false;// 是否已经购买


  var swiperDataList = [
    
  ];
  var UserMoneyAndAsset;
  var UnderStandNowleList;
  var SearchDemandsist=[];

  AnimationController controllerHeader;
  AnimationController controllerFooter;

  var topList = [];

  Timer _countdownTimer;
  int _countdownNum = 0;

  @override
  void dispose() {
    _countdownTimer?.cancel();
    _countdownTimer = null;
    super.dispose();
  }

  @override
  void initState() {
    _controller = EasyRefreshController();
    Tabcontroller = TabController(length: 1, vsync: this);
    //  pullInfocontroller = new AnimationController(vsync: this);
    //  pullInfocontroller = new AnimationController(vsync: this,duration: new Duration(milliseconds: 1000));
    //  pullInfoanimation = new Tween(begin: 0.0,end: 50.0).animate(pullInfocontroller)
    //      ..addListener((){
    //        setState(() {
    //    });
    //  });
    //获取轮播图
    SearchDemands();
    GetScrollMessages();
    SearchIndexRecommendGoods();
    GetUnderStandNowleList();
    SearchGoodsType();
    // 获取用户位置信息
    getString();

    super.initState();
  }

  Widget buildImage(String imageUrl) {
    if (imageUrl.indexOf("http://") > -1 || imageUrl.indexOf("https://") > -1) {
      return CachedNetworkImage(
        width: MediaQuery.of(context).size.width,
        height: 400,
        imageUrl: imageUrl,
        imageBuilder: (context, imageProvider) => Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.cover,
              /*colorFilter: ColorFilter.mode(Colors.red, BlendMode.colorBurn)*/
            ),
          ),
        ),
        fit: BoxFit.fitHeight,
        placeholder: (context, url) {
          return Container(child: CircularProgressIndicator());
        },
        errorWidget: (context, url, error) => Icon(Icons.error),
      );
    } else {
      return Image.asset(
        imageUrl,
        width: MediaQuery.of(context).size.width,
        height: 400,
        fit: BoxFit.fitHeight,
      );
    }
  }



  // 判断是否是网络地址
  bool isHttpUrl(String url) {
    if (url == null) return false;
    if (url.indexOf("http://") > -1 || url.indexOf("https://") > -1) {
      return true;
    }
    return false;
  }


//获取产品列表
  void SearchDemands() {


    var url = Api.API_URL + '/service-v2/searchDemands';
    var postdata = {
      "demandName": '',
      "goodsId": 0,
      "orderByType": 1,
      "type": '',
      "page": 1,
      "size": 2
    };
    print("开始加载产品");
    print(postdata);
    Request().post(url, data: postdata).then((data) {
//      print(" data['data']['records']   ${data['data']}  ");
      if (data != null && data['error'] == 0) {
        
        setState(() {
                    SearchDemandsist = data['data']['records'];
                  });
                  
      } else {
       
      }
    });
  }
  // 获取商品
  Future<dynamic> SearchIndexRecommendGoods({bool isInit = false}) async {
    var url = Api.API_URL + '/service-v2/searchIndexRecommendGoods';
    print(url);
    return Request().get(url).then((data){
          print(data);
          initAdv = true;
          if (data['error'] == 0)
            {RecommendGoodsList = data['data'];};
        });
  }

  /**
   * 检查URL中是否含有Token
   **/
  String checkUrl(String url) {
    if (url == null) return "about:blank";
    if (url.indexOf("{{token}}") > -1) {
      url = url.replaceAll("{{token}}", _token);
    }
    return url;
  }

  /**
   * 检查标题是否为空
   */
  String checkTitle(String title) {
    if (title == null || title == "") return "麻利活动";
    return title;
  }
  

  Future saveBoolean(property, message) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setBool(property, message);
  }

  Future getString() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var login_bool = sharedPreferences.getBool('login');
    if (login_bool == true) {
      setState(() {
        shell_true = true;
        _token = sharedPreferences.get('token');
      });
      Tolocation();
      GetUserIdentification();
      GetUserMoneyAndAsset();
      getMessagesList();
    } else {
      
    }
  }

  //选择栏目
  Future<void> _showWXZcontract() async {
    return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return GestureDetector(
              child: Material(
                type: MaterialType.transparency,
                child: Center(
                  child: GestureDetector(
                    child: Container(
                      // margin: EdgeInsets.fromLTRB(widgetFit.unit * 34, 0, 0, 0),
                      child: Column(
                        children: <Widget>[
                          Container(
                            child: Row(
                              children: <Widget>[
                                GestureDetector(
                                  child: Container(
                                    color: Colors.white,
                                    margin: EdgeInsets.fromLTRB(
                                        widgetFit.unit * 22, 0, 0, 0),
                                    padding:
                                        EdgeInsets.all(widgetFit.unit * 22),
                                    child: Icon(
                                      MyIconFont.shanchu,
                                      size: widgetFit.unit * 24,
                                      color: Color(0xFF333333),
                                    ),
                                  ),
                                  onTap: () => {Navigator.pop(context)},
                                ),
                                Container(
                                  margin: EdgeInsets.fromLTRB(
                                      widgetFit.unit * 218, 0, 0, 0),
                                  child: Text('全部频道',
                                      style: TextStyle(
                                          fontSize: widgetFit.unit * 34,
                                          fontWeight: FontWeight.w500)),
                                )
                              ],
                            ),
                            color: Colors.white,
                            height: widgetFit.unit * 88,
                          ),
                          Container(
                              margin: EdgeInsets.fromLTRB(
                                  0, widgetFit.unit * 24, 0, 0),
                              child: Wrap(
                                  spacing: widgetFit.unit * 24,
                                  runSpacing: widgetFit.unit * 24,
                                  alignment: WrapAlignment.start,
                                  children: worker<Widget>(GoodsTypeList,
                                      (index, item) {
                                    return GestureDetector(
                                      child: Container(
                                        alignment: Alignment.center,
                                        child: Text('${item['typeName']}',
                                            style: TextStyle(
                                                fontSize: widgetFit.unit * 26)),
                                        width: widgetFit.unit * 218,
                                        height: widgetFit.unit * 88,
                                        decoration: BoxDecoration(
                                            color: Color(0xFFFFFFFF),
                                            borderRadius: BorderRadius.circular(
                                                widgetFit.unit * 44)),
                                      ),
                                      onTap: () => {
                                        ClearPage(),
                                        SearchGoods('', item['id']),
                                        Navigator.pop(context),
                                        Tabcontroller.index = index
                                      },
                                    );
                                  }))),
                        ],
                      ),
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      decoration: BoxDecoration(
                          color: Color(0xFFF8F8F8),
                          borderRadius:
                              BorderRadius.circular(widgetFit.unit * 8)),
                    ),
                    onTap: () => false,
                  ),
                ),
              ),
              onTap: () => {});
        });
  }

  // 清空当前页数
  ClearPage() {
    setState(() {
      GoodsListPage = 1;
    });
  }

//是否认证
  void GetUserIdentification() {
    var url = Api.API_URL + '/service-v2/getUserIdentification?token=' + _token;
    // print(url);
    Request().get(url).then((data){
          // print(data),
          if (data['error'] == 0)
            {
              if (data['data'] == 1)
                {
                  IsDealRentGoods();
                  setState(() {
                    UserIdentificationBool = false;
                  });
                }
              else
                {
                  setState(() {
                    UserIdentificationBool = true;
                  });
                }
            }
        });
  }

  //是否有待出租的资产
  void IsDealRentGoods() {
    var url = Api.API_URL + '/service-v2/isDealRentGoods?token=' + _token;
    // print(url);
    Request().get(url).then((data){
          // print(data),
          if (data['error'] == 0)
            {
              if (data['data'] == 1)
                {
                  setState(() {
                    DealRentGoodsBool = true;
                  });
                }
              else
                {
                  setState(() {
                    DealRentGoodsBool = false;
                  });
                }
            }
        });
  }

// 获取用户位置信息
  void Tolocation() {
    var url = Api.API_URL + '/service/getLocation?token=' + _token;
    // print(url);
    Request().get(url).then((data){
          if (data['error'] == 0) {}
        });
  }

  //获取商品列表
  void SearchGoods(goodsName, typeId) {
    var list = GoodsList;
    var url = Api.API_URL + '/service-v2/searchGoods';
    // print(url);
    var postdata = {
      'goodsName': goodsName,
      'page': GoodsListPage,
      'size': 8,
      'typeId': typeId,
    };
    // print(postdata);
    Request().post(url, data: postdata).then((data){
          // print(data),
          setState(() {
            isPerformingRequest = false;
          });
          if (data['error'] == 0)
            {
              {
                if (data['data']['records'].length > 0)
                  {
                    //验证是否推荐频道
                    if (typeId == 0)
                      {
                        //如果不是分页第1页
                        if (GoodsListPage != 1)
                          {
                            if (GoodsListPage == 2 || GoodsListPage == 3)
                              {
                                for (var i = 0;
                                    i < data['data']['records'].length;
                                    i++)
                                  {
                                    if (i == 2)
                                      {
                                        if (list.length == 10 &&
                                            GoodsListPage == 2)
                                          {list.add(AdsList[1]); i--;}
                                        else if (list.length == 18 &&
                                            GoodsListPage == 3)
                                          {i--; list.add(AdsList[2]);}
                                      }
                                    else
                                      {list.add(data['data']['records'][i]);}
                                  }
                              }
                            else
                              {
                                for (var i = 0;
                                    i < data['data']['records'].length;
                                    i++)
                                  {list.add(data['data']['records'][i]);};
                              }
                          }
                        else
                          {
                            list = [];
                            for (var i = 0;
                                i < data['data']['records'].length;
                                i++)
                              {
                                if (list.length == 2 && AdsList.length > 0)
                                  {list.add(AdsList[0]); i--;}
                                else
                                  {list.add(data['data']['records'][i]);}
                              }
                          }
                        setState(() {
                          GoodsListSize = data['data']['size'];
                          GoodsList = list;
                        });
                      }
                    else
                      {
                        if (GoodsListPage != 1)
                          {
                            for (var i = 0;
                                i < data['data']['records'].length;
                                i++)
                              {list.add(data['data']['records'][i]);};
                          }
                        else
                          {list = data['data']['records'];};
                        setState(() {
                          GoodsListSize = data['data']['size'];
                          GoodsList = list;
                        });
                      }
                  }
                else if (GoodsListPage == 1 &&
                    data['data']['records'].length <= 0)
                  {
                    setState(() {
                      GoodsList = [];
                    });
                  }
              }
            }
          else
            {prefix1.Toast.show(context, data['msg']);}
        });
  }

  // 查询所有商品类型
  void SearchGoodsType() {
    var url = Api.API_URL + '/service-v2/searchGoodsType';
    Request().get(url).then((data) {
      if (data['error'] == 0) {
        setState(() {
          GoodsTypeList = data['data'];
        });
        Tabcontroller = TabController(length: data['data'].length, vsync: this);
      }
    });
  }

  // 获取滚动轮播
  void GetScrollMessages() {
    var url = Api.API_URL + '/service-v2/getAds?type=5';
    Request().get(url).then((data) {
      if (data['error'] == 0) {
        setState(() {
          swiperDataList = data['data'];
        });
        print('轮播轮播轮播轮播轮播轮播轮播轮播');
        print(swiperDataList);
      }
    });
  }
  // 了解麻利
  void GetUnderStandNowleList() {
    var url = Api.API_URL + '/service-v2/getUnderStandNowleList';
    Request().get(url).then((data) {
      if (data['error'] == 0) {
        setState(() {
          UnderStandNowleList = data['data'];
        });
      }
    });
  }
  
  // 首页统计-租金 and 资产
  void GetUserMoneyAndAsset() {
    var url = Api.API_URL + '/service-v2/getUserMoneyAndAsset?token='+_token;
    Request().get(url).then((data) {
      if (data['error'] == 0) {
        print('首页统计-租金 and 资产');
        setState(() {
          UserMoneyAndAsset = data['data'];
        });
        

        var newData=data['data'];
        print(newData); //

        if((newData!=null && newData['waitAssets']!=null&&newData['waitAssets']!=''&&newData['waitAssets']==0) || (newData!=null && newData['waitAssets']==null)){
          if((newData!=null && newData['assetings']!=null&&newData['assetings']!=''&&newData['assetings']==0) || (newData!=null && newData['assetings']==null)){
            setState(() {
              isAlBuyBool = true;
            });
          }else{
            setState(() {
              isAlBuyBool = false;
            });
          }
        }else{
          setState(() {
            isAlBuyBool = false;
          });
        }
      }
    });
  }

  // 刷新数据
  void refreshData() {
    GetUserIdentification();
    IsDealRentGoods();
  }

  Widget buildNotice() {
    //AssetAlterPositionBol = true;
    return AssetAlterPositionBol
        ? Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.fromLTRB(widgetFit.unit * 32, widgetFit.unit * 10.0, widgetFit.unit * 32, 0),
            child: UserIdentificationBool
                ? GestureDetector(
                    child: Container(
                        alignment: Alignment.centerLeft,
                        padding:
                            EdgeInsets.fromLTRB(widgetFit.unit * 48, 0, 0, 0),
                        width: widgetFit.unit * 686,
                        height: widgetFit.unit * 70,
                        decoration: BoxDecoration(
                            color: Color(0xFFFF6A38),
                            borderRadius:
                                BorderRadius.circular(widgetFit.unit * 44)),
                        child: Row(
                          children: <Widget>[
                            Container(
                              width: widgetFit.unit * 600,
                              child: Text(
                                '您尚未实名认证，快来实名认证领取奖励！',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: widgetFit.unit * 26),
                              ),
                            ),
                            Container(
                              child: Icon(
                                MyIconFont.go,
                                color: Colors.white,
                                size: widgetFit.unit * 24,
                              ),
                            )
                          ],
                        )),
                    onTap: () => {
                      
                    },
                  )
                : GestureDetector(
                    child: DealRentGoodsBool
                        ? Container(
                            alignment: Alignment.centerLeft,
                            padding: EdgeInsets.fromLTRB(
                                widgetFit.unit * 48, 0, 0, 0),
                            width: widgetFit.unit * 686,
                        height: widgetFit.unit * 70,
                            decoration: BoxDecoration(
                                color: Color(0xFFFF6A38),
                                borderRadius:
                                    BorderRadius.circular(widgetFit.unit * 44)),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  width: widgetFit.unit * 600,
                                  child: Text(
                                    '您有资产尚未出租，出租资产可获得租金',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: widgetFit.unit * 26),
                                  ),
                                ),
                                Container(
                                  child: Icon(
                                    MyIconFont.go,
                                    color: Colors.white,
                                    size: widgetFit.unit * 24,
                                  ),
                                )
                              ],
                            ))
                        : Container(),
                   
                  ),
          )
        : SizedBox(height: 0);
  }

  Widget buildSelectGoods() {//精选商品
    return  Container(
      color: Colors.white,
      width: MediaQuery.of(context).size.width,
      height: widgetFit.unit * 420+50+10,
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(17,0, 0,0),
            width: MediaQuery.of(context).size.width,
            height: 50.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('精选商品',style: new TextStyle(fontSize: widgetFit.unit * 36,fontWeight: FontWeight.w500),),
                GestureDetector(
                  child:  Container(
                    width: 80,
                    height: widgetFit.unit * 40.0,
                    child: Row(
                      children: <Widget>[
                        Text('查看更多',style: new TextStyle(fontSize: 14,color: Color(0xFF999999))),
                        Container(
                          child: Icon(
                            MyIconFont.go,
                            color: Color(0xFF999999),
                            size: widgetFit.unit * 24,
                          ),
                        )
                      ],
                    ),
                  ),
                  
                ),

              ],
            ),
          ),
          Container(
              padding: EdgeInsets.fromLTRB(10,0, 0,0),
              width: MediaQuery.of(context).size.width,
              height: widgetFit.unit * 420,
              child: RecommendGoodsList.length>0?new ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount:3,
                itemBuilder: (context, index) => initGoodsItem(index),
              ):Container()
          ),
        ],
      ),
    );
  }
  Widget buildSelectDemand() { //精选需求
      return Container(
        width: MediaQuery.of(context).size.width,
        height: widgetFit.unit * 70.0+(widgetFit.unit * 255+20)*2+10,

        margin: EdgeInsets.fromLTRB(0, 0, 0,0),
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(17,0, 0,0),
              width: MediaQuery.of(context).size.width,
              height: 50.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('精选需求',style: new TextStyle(fontSize: widgetFit.unit * 36,fontWeight: FontWeight.w500),),
                  GestureDetector(
                    child:  Container(
                      width: 80,
                      height: widgetFit.unit * 40.0,
                      child: Row(
                        children: <Widget>[
                          Text('查看更多',style: new TextStyle(fontSize: 14,color: Color(0xFF999999))),
                          Container(
                            child: Icon(
                              MyIconFont.go,
                              color: Color(0xFF999999),
                              size: widgetFit.unit * 24,
                            ),
                          )
                        ],
                      ),
                    ),
                    
                  ),

                ],
              ),
            ),
            SearchDemandsist.length>=1?initDemandItem(SearchDemandsist[0]):Container(),
            SearchDemandsist.length>=2?initDemandItem(SearchDemandsist[1]):Container()
          ],
        ),
      );
  }

  Future<Null> _onRefresh() async {
    return Future.delayed(Duration(seconds: 2), () {
      print('下拉刷新');
      setState(() {
        GoodsListPage = 1;
      });
      SearchIndexRecommendGoods();
      GetUserMoneyAndAsset();
      GetUserIdentification();
      IsDealRentGoods();
      GetScrollMessages();
      SearchDemands();
      //_controller.resetLoadState();
    });
  }

  Future<Null> _onLoadMore() async {
    return Future.delayed(Duration(seconds: 2), () {
      print('上拉加载');
      setState(() {
        GoodsListPage = GoodsListPage + 1;
      });
      SearchGoods('', GoodsTypeIndex);
      //_controller.finishLoad(noMore: GoodsList.length >= GoodsListSize);
    });
  }

  Future<Null> onFooterRefresh() async {
    print("上拉加载");
    // 未出租资产提示是否提醒
    /* if (DealRentGoodsBool) {
      if (_scrollController.position.pixels > 0) {
        setState(() {
          AssetAlterPositionBol = true;
        });
      } else {
        setState(() {
          AssetAlterPositionBol = true;
        });
      }
    }
    */

    isPerformingRequest = true;
    return _onLoadMore();
  }

  Future<Null> onHeaderRefresh() async {
    print("下拉刷新");
    return _onRefresh();
  }

  // 显示商品列表
  Widget buildProducts() {
    return new Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(0),
        color: Color(0xFFF6F6F6),
        child: new Column(
          children: <Widget>[
            Wrap(
                spacing: 0, // gap between adjacent chips
                runSpacing: 0.0, // gap between lines
                runAlignment: WrapAlignment.start,
                alignment: WrapAlignment.start,
                crossAxisAlignment: WrapCrossAlignment.start,
                children: worker<Widget>(GoodsList, (index, goods) {
                  return new Container(
                      width: (MediaQuery.of(context).size.width / 2) - 10,
                      height: 257,
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      margin: index % 2 == 0
                          ? EdgeInsets.fromLTRB(5, 2.5, 2.5, 2.5)
                          : EdgeInsets.fromLTRB(2.5, 2.5, 5, 2.5),
                      decoration: BoxDecoration(
                        color: Color(0xFFFFFFFF),
                        border: index < 2
                            ? Border(
                                top: BorderSide(
                                    color: Color(0xFFEDEDED), width: 0),
                                bottom: BorderSide(
                                    color: Color(0xFFEDEDED), width: 0),
                                right: BorderSide(
                                    color: Color(0xFFEDEDED), width: 0))
                            : Border(
                                bottom: BorderSide(
                                    color: Color(0xFFEDEDED), width: 0),
                                right: BorderSide(
                                    color: Color(0xFFEDEDED), width: 0)),
                      ),
                      child: GoodsList[index]['imgUrl'] != null &&
                              GoodsList[index]['imgUrl'] != ''
                          ? GestureDetector(
                              child: Container(
                                margin: index > 2
                                    ? EdgeInsets.fromLTRB(
                                        0, widgetFit.unit * 10, 0, 0)
                                    : EdgeInsets.fromLTRB(0, 0, 0, 0),
                                //随机生成高度
                                //height: widgetFit.unit * 700,
                                child: GoodsList[index]['imgUrl'] != null &&
                                        GoodsList[index]['imgUrl'] != ''
                                    ? Image.network(Compatible.Http(
                                        '${GoodsList[index]['imgUrl']}'))
                                    : Container(),
                              ),
                              
                            )
                          : Container(
                              margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                              // 随机生成高度
                              height: widgetFit.unit * 508,
                              child: GestureDetector(
                                child: Container(
                                  // alignment: Alignment.center,
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        height: widgetFit.unit * 342,
                                        decoration: GoodsList[index]['image'] !=
                                                    null &&
                                                GoodsList[index]['image'] != ''
                                            ? BoxDecoration(
                                                image: DecorationImage(
                                                    image: NetworkImage(
                                                        Compatible.Http(
                                                            GoodsList[index]
                                                                ['image'])),
                                                    fit: BoxFit.fitWidth),
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        widgetFit.unit * 12))
                                            : BoxDecoration(),
                                        // color: Colors.black45,
                                      ),
                                      Container(
                                        margin: EdgeInsets.fromLTRB(
                                            widgetFit.unit * 16,
                                            widgetFit.unit * 20,
                                            widgetFit.unit * 16,
                                            0),
                                        alignment: Alignment.centerLeft,
                                        width:
                                            MediaQuery.of(context).size.width -
                                                widgetFit.unit * 32,
                                        child: Text(
                                          GoodsList[index]['goodsName'] !=
                                                      null &&
                                                  GoodsList[index]
                                                          ['goodsName'] !=
                                                      ''
                                              ? '${GoodsList[index]['goodsName']}'
                                              : '',
                                          style: TextStyle(
                                              color: Color(0xFF333333),
                                              fontSize: widgetFit.unit * 30,
                                              fontWeight: FontWeight.w500),
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.fromLTRB(
                                            widgetFit.unit * 16,
                                            widgetFit.unit * 10,
                                            widgetFit.unit * 16,
                                            0),
                                        alignment: Alignment.centerLeft,
                                        width:
                                            MediaQuery.of(context).size.width -
                                                widgetFit.unit * 32,
                                        child: Text(
                                            GoodsList[index]['price'] != null &&
                                                    GoodsList[index]['price'] !=
                                                        ''
                                                ? '￥${GoodsList[index]['price']}'
                                                : '',
                                            style: TextStyle(
                                                color: Color(0xFFFF8527),
                                                fontSize: widgetFit.unit * 30,
                                                fontWeight: FontWeight.w500),
                                            overflow: TextOverflow.ellipsis),
                                      ),
                                      Container(
                                        margin: EdgeInsets.fromLTRB(
                                            widgetFit.unit * 16,
                                            widgetFit.unit * 10,
                                            widgetFit.unit * 16,
                                            0),
                                        alignment: Alignment.centerLeft,
                                        width:
                                            MediaQuery.of(context).size.width -
                                                widgetFit.unit * 32,
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                              child: Image.asset(
                                                'assets/image/service_two/company@2x.png',
                                                width: widgetFit.unit * 24,
                                                height: widgetFit.unit * 21,
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.fromLTRB(
                                                  widgetFit.unit * 8, 0, 0, 0),
                                              child: Text(
                                                  GoodsList[index][
                                                                  'production'] !=
                                                              null &&
                                                          GoodsList[index]
                                                                  [
                                                                  'production'] !=
                                                              ''
                                                      ? '${GoodsList[index]['production']}'
                                                      : '',
                                                  style: TextStyle(
                                                      color: Color(0xFF999999),
                                                      fontSize:
                                                          widgetFit.unit * 24),
                                                  overflow:
                                                      TextOverflow.ellipsis),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                  width: widgetFit.unit * 342,
                                  height: widgetFit.unit * 508,
                                  decoration: BoxDecoration(
                                      color: Color(0xFFFFFFFF),
                                      borderRadius: BorderRadius.circular(
                                          widgetFit.unit * 10)),
                                ),
                                onTap: () => {
                                  
                                },
                              ),
                            ));
                }))
          ],
        ));
  }

  // 显示商品列表
  Widget buildGoodsList() {
    // DealRentGoodsBool = true;
    //UserIdentificationBool = true;
    return Column(
      children: <Widget>[buildNotice(), buildProducts()],
    );
  }

  Widget buildProgressIndicator() {
    return new Container(
      width: MediaQuery.of(context).size.width,
      height: widgetFit.unit * 44,
      child: new Center(
        child: new Opacity(
          opacity: isPerformingRequest ? 1.0 : 0.0,
          child: new CircularProgressIndicator(
              valueColor: ColorTween(
            begin: Color(0xFFFF8527),
            end: Color(0xFFFF8527),
          ).animate(
            CurvedAnimation(
              parent: controllerFooter,
              curve: Interval(
                0.0,
                0.0,
                curve: Curves.linear,
              ),
            ),
          )),
        ),
      ),
    );
  }

  Widget buildBanner() {
    return ClipRRect(
        borderRadius: BorderRadius.circular(4.0),
        child: Container(
            width: MediaQuery.of(context).size.width - 20,
            height: 110,
            margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
            alignment: Alignment.center,
            child: swiperDataList.length>0?Swiper(
              itemCount: swiperDataList.length,
              itemBuilder: (BuildContext context, int index) {
                return 
                    GestureDetector(
                      child:Image.network(Compatible.Http(swiperDataList[index]['imgUrl']),
                    fit: BoxFit.fill),
                      
                    );
              },
              pagination: SwiperPagination(),
              autoplay: true,
            ):Container()));
  }

  // 商品无数据时显示
  Widget buildActions() {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: GestureDetector(
                
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(4.0),
                    child: Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 5, 5),
                      alignment: Alignment.center,
                      height: isAlBuyBool==true ? 140 : 110,
                      decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(widgetFit.unit * 10),
                        gradient: LinearGradient(
                          colors: [Color(0xFF333333), Color(0xFF8D8D8D)],
                          begin: Alignment.bottomLeft,
                          end: Alignment.topRight,
                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: (MediaQuery.of(context).size.width-30)/2,
                            height: 30,
                            padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                            alignment: Alignment.centerLeft,
                            child: Row(
                              children: <Widget>[
                                Text(shell_true == true ? "我的租金" :"赚租金" , style: TextStyle(fontSize: widgetFit.unit *32, color: Color(0xFFFFFFFF))),
                                shell_true == true ? GestureDetector(
                                  onTap: (){
                                    setState(() {
                                      isOpenEyeBool = !isOpenEyeBool;
                                    });
                                    return;
                                  },
                                  child: Container(
                                    width: 50/2,
                                    height: isOpenEyeBool == false ? 37/2 : 28/2,
                                    margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                    child: Image.asset(isOpenEyeBool == false ? 'assets/image/homeone_eye.png': 'assets/image/homeont_eye_close.png',fit: BoxFit.scaleDown),
                                  ),
                                ) : Container(),
                              ],
                            ),
                          ),
                          Container(
                            width: (MediaQuery.of(context).size.width-30)/2,
                            height: 30,
                            padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                            alignment: Alignment.centerLeft,
                            child: Text(shell_true == false ? "精选安全优质需求" : UserMoneyAndAsset!=null && UserMoneyAndAsset['yestodayMoney']!=null&& UserMoneyAndAsset['yestodayMoney']!=''? isOpenEyeBool == true ? "昨日租金 **** 元" : "昨日租金 ${UserMoneyAndAsset['yestodayMoney']}元":'昨日租金 0 元',
                                style: TextStyle(
                                    fontSize: widgetFit.unit *26, color: Color(0xFFFFFFFF))),
                          ),
                          Container(
                            width: (MediaQuery.of(context).size.width-30)/2,
                            height: 30,
                            padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                            alignment: Alignment.centerLeft,
                            child: Text(shell_true == false ? "高租金 有保障" : UserMoneyAndAsset!=null && UserMoneyAndAsset['totalMoney']!=null&&UserMoneyAndAsset['totalMoney']!=''? isOpenEyeBool == true ? "总收益 **** 元" :"总收益${UserMoneyAndAsset['totalMoney']}元":'总收益 0 元',
                                style: TextStyle(fontSize: widgetFit.unit *26,color: Color(0xEEEEEEEE))),
                          ),

                          isAlBuyBool==true ?  GestureDetector(
                            
                            child: Container(
                              width: (MediaQuery.of(context).size.width-30)/2-40,
                              height: 30,
                              alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(20, 5, 20, 0),
                              child: Text("赚钱", style: TextStyle(fontSize: widgetFit.unit *30, color: Color(0xFF4B4B4B))),
                              decoration: BoxDecoration(
                                  color: Color(0xFFFFFFFF),
                                  borderRadius: BorderRadius.circular(widgetFit.unit * 10)
                              ),
                            ),
                          ): Container()
                        ],
                      ),
                    ))),
          ),
          Expanded(
            flex: 1,
            child: GestureDetector(
                onTap: () {
                  if(shell_true == false ){
                    
                  }else{
//                    TuanYService.eventBus.fire(ChangeEvent(1));
                    
                  }
                },
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(4.0),
                    child: Container(
                      margin: EdgeInsets.fromLTRB(5, 10, 10, 5),
                      alignment: Alignment.center,
                      height: isAlBuyBool==true ? 140 : 110,
                      decoration: BoxDecoration(
                          //color: Color(0xDDFBB68F),
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(widgetFit.unit * 10),
                          gradient: LinearGradient(
                            colors: [Color(0xFFFF6B38), Color(0xFFFF9430)],
                            begin: Alignment.bottomLeft,
                            end: Alignment.topRight,
                          ),

                          ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: (MediaQuery.of(context).size.width-30)/2,
                            height: 30,
                            padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                            alignment: Alignment.centerLeft,
                            child: Row(
                              children: <Widget>[
                                Text("我的资产", style: TextStyle(fontSize: widgetFit.unit *32, color: Color(0xFFFFFFFF))),
                              ],
                            ),
                          ),
                          Container(
                            width: (MediaQuery.of(context).size.width-30)/2,
                            height: 30,
                            padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                            alignment: Alignment.centerLeft,
                            child: Text(shell_true == false ? "筛选合适商品" : UserMoneyAndAsset!=null && UserMoneyAndAsset['waitAssets']!=null&&UserMoneyAndAsset['waitAssets']!=''?"待出租 ${UserMoneyAndAsset['waitAssets']} 件":'待出租 0 件',
                                style: TextStyle(fontSize: widgetFit.unit *26, color: Color(0xFFFFFFFF))),
                          ),
                          Container(
                            width: (MediaQuery.of(context).size.width-30)/2,
                            height: 30,
                            padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                            alignment: Alignment.centerLeft,
                            child: Text(shell_true == false ? "操作灵活 随租随售" : UserMoneyAndAsset!=null && UserMoneyAndAsset['assetings']!=null&&UserMoneyAndAsset['assetings']!=''?"出租中 ${UserMoneyAndAsset['assetings']} 件":'出租中 0件',
                                style: TextStyle(fontSize: widgetFit.unit *26,color: Color(0xEEEEEEEE))),
                          ),
                          isAlBuyBool==true ? GestureDetector(
                            
                            child: Container(
                              width: (MediaQuery.of(context).size.width-30)/2-40,
                              height: 30,
                              alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(20,5, 20, 0),
                              child: Text("购买", style: TextStyle(fontSize: widgetFit.unit *30, color: Color(0xFFFF6A38))),
                              decoration: BoxDecoration(
                                  color: Color(0xFFFFFFFF),
                                  borderRadius: BorderRadius.circular(widgetFit.unit * 10)
                              ),
                            ),
                          ): Container()
                        ],
                      ),
                    ))),
          )
        ]);
  }

  // 商品无数据时显示
  Widget buildEmptyBox() {
    return Container(
      height: MediaQuery.of(context).size.height - 168,
      child: Column(
        children: <Widget>[
          Container(
            width: widgetFit.unit * 520,
            height: widgetFit.unit * 208,
            margin: EdgeInsets.fromLTRB(0, widgetFit.unit * 350, 0, 0),
            child: Image.asset('assets/image/service_two/NoGoodList.png',
                fit: BoxFit.fill),
          ),
          Container(
            child: Text('更多商品即将来袭，敬请期待！',
                style: TextStyle(
                    color: Color(0xFFAEB0BD), fontSize: widgetFit.unit * 30)),
            margin: EdgeInsets.fromLTRB(0, widgetFit.unit * 94, 0, 0),
          )
        ],
      ),
    );
  }

  // 显示搜索框
  Widget buildSearchBox() {
    return Container(
      color: Colors.white,
      width: MediaQuery.of(context).size.width,
      height: widgetFit.unit * 88,
      child: new Row(
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(widgetFit.unit * 0, widgetFit.unit * 5, widgetFit.unit * 5, widgetFit.unit * 5),
            width: widgetFit.unit * 64,
            height: widgetFit.unit * 64,
            child: Image.asset(
              'assets/image/slogo.png',
              width: widgetFit.unit * 64,
              height: widgetFit.unit * 64,
              fit: BoxFit.fitWidth,
            ),
          ),
          GestureDetector(
            child: new Container(
              child: new Row(
                children: <Widget>[
                  new Icon(
                    MyIconFont.search,
                    color: Color(0xFFb5b5b5),
                    size: widgetFit.unit * 34,
                  ),
                  new Container(
                    child: Text('人民出行',
                        style: TextStyle(
                            color: Color(0xFFb5b5b5),
                            fontSize: widgetFit.unit * 26.0)),
                    padding:
                        new EdgeInsets.fromLTRB(widgetFit.unit * 16, 0, 0, 0),
                  ),
                ],
              ),
              width: widgetFit.unit * 510 - 48-10,
              height: widgetFit.unit * 66.0,
              padding: new EdgeInsets.fromLTRB(widgetFit.unit * 46, 0, 0, 0),
              margin: EdgeInsets.fromLTRB(
                  widgetFit.unit * 24, widgetFit.unit * 0.0, 0, 0),
              decoration: new BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      color: Color(0xFFe8e8e8),
                      offset: Offset(1.0, 1.0),
                      blurRadius: widgetFit.unit * 10.0,
                      spreadRadius: widgetFit.unit * 0.1),
                ],
                borderRadius: BorderRadius.circular(widgetFit.unit * 33),
              ),
            ),
            
          ),
          GestureDetector(
              child: Container(
                //padding: EdgeInsets.all(5),
                child: Image.asset(
                  'assets/image/service_two/list@2x.png',
                  fit: BoxFit.fill,
                ),
                // color: Colors.black45,
                //width: widgetFit.unit * 64,
                height: widgetFit.unit * 64,
                margin: new EdgeInsets.fromLTRB(widgetFit.unit * 22, 0, 0, 0),
              ),
              ),
          GestureDetector(
              child: Container(
                //padding: EdgeInsets.all(5),
                child: Image.asset(
                  'assets/image/service_two/con@2x.png',
                  fit: BoxFit.fill,
                ),
                // color: Colors.black45,
                //width: widgetFit.unit * 64,
                height: widgetFit.unit * 64,
                margin: new EdgeInsets.fromLTRB(widgetFit.unit * 22, 0, 0, 0),
              ),
              )
        ],
      ),
    );
  }

  // 显示分类导航栏
  Widget buildTabBar() {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            height: widgetFit.unit * 90,
            child: Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(0, widgetFit.unit * 20, 0, 0),
                  padding: EdgeInsets.fromLTRB(widgetFit.unit * 0, 0, 0, 0),
                  alignment: Alignment.bottomCenter,
                  child: CustomerTabBar(
                      controller: Tabcontroller,
                      unselectedLabelColor: Color(0xFF333333),
                      indicatorPadding: EdgeInsets.fromLTRB(
                          widgetFit.unit * 80, 2, widgetFit.unit * 80, 2),
                      indicatorColor: Color(0xFFFF8527),
                      isScrollable: true,
                      indicatorSize: TabBarIndicatorSize.label, //设置成lable的长度
                      indicatorWeight: widgetFit.unit * 24,
                      labelPadding: EdgeInsets.fromLTRB(
                          widgetFit.unit * 28, 0, widgetFit.unit * 28, 0),
                      labelStyle: TextStyle(
                          fontSize: widgetFit.unit * 36,
                          fontWeight: FontWeight.w500),
                      unselectedLabelStyle: TextStyle(
                          fontSize: widgetFit.unit * 26,
                          fontWeight: FontWeight.w500),
                      tabs: GoodsTypeList.length > 0
                          ? worker<Widget>(GoodsTypeList, (index, item) {
                              return CustomerTab(
                                child: Text('${item['typeName']}'),
                              );
                            })
                          : <Widget>[CustomerTab(child: Text(''))]),
                  width: widgetFit.unit * 626,
                ),
                GestureDetector(
                  child: Container(
                    child: Row(
                      children: <Widget>[
                        Container(
                            margin: EdgeInsets.fromLTRB(
                                widgetFit.unit * 30, 0, 0, 0),
                            child: Image.asset(
                              'assets/image/service_two/more@2x.png',
                              width: widgetFit.unit * 64,
                              height: widgetFit.unit * 64,
                            ))
                      ],
                    ),
                    width: widgetFit.unit * 124,
                  ),
                  onTap: () => {_showWXZcontract()},
                )
              ],
            ),
          )
        ],
      ),
      width: MediaQuery.of(context).size.width,
      // height: widgetFit.unit * 192.0,
      color: Colors.white,
      margin: new EdgeInsets.fromLTRB(0, 0, 0, 0),
    );
  }

  static SlideTransition createTransition(
      Animation<double> animation, Widget child) {
    return new SlideTransition(
      position: new Tween<Offset>(
        begin: const Offset(1.0, 0.0),
        end: const Offset(0.0, 0.0),
      ).animate(animation),
      child: child,
    );
  }

  showtMessagesList(var list) async {
    setState(() {
      if (_countdownTimer != null) {
        return;
      }
      // Timer的第一秒倒计时是有一点延迟的，为了立刻显示效果可以添加下一行。
      _countdownTimer = new Timer.periodic(new Duration(seconds: 5), (timer) {
        setState(() {
          if (_countdownNum > 0) {
            _countdownNum--;
            prefix1.Toast.show(context, topList[_countdownNum]['content']);
            print("_countdownNum 0000   ${topList[_countdownNum]['id']}");

            updateMessagesById(topList[_countdownNum]['id']);
          } else {
            _countdownNum = 0;
            _countdownTimer.cancel();
            _countdownTimer = null;
          }
        });
      });
    });
  }

// 获取广告
  getMessagesList() async {
    var url = Api.API_URL + '/service-v2/getMessagesList?token=' + _token;
    Request().get(url).then((data){
          print(' getMessagesList @@@@@@@@@@@@@@@@@');
          if (data['error'] == 0)
            {
              topList = data['data'];
              _countdownNum = data['data'].length;
              showtMessagesList(data['data']);
              print('@@@@@@@  ++++   @@@@@@@@@@');
            }
        });
  }

  // 设置已读
  updateMessagesById(var idStr) async {
    var url =
        Api.API_URL + '/service-v2/updateMessagesById?id=' + idStr.toString();
    Request().get(url).then((data){
          if (data['error'] == 0)
            {
              print(' updateMessagesById 成功');
            };
        });
  }
//  精选商品 item
  initGoodsItem(int index) {
    var GoodsList=RecommendGoodsList;
    return Container(
      margin: EdgeInsets.fromLTRB(widgetFit.unit * 20,0, widgetFit.unit * 0,0),
      width: widgetFit.unit * 277,
      height: widgetFit.unit * 390,
//color: Colors.yellow,
      padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
        child: Container(
          decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                    color: Color(0xFFF0F0F0),
                    offset: Offset(1.0, 2.0),
                    blurRadius: widgetFit.unit * 2.0,
                    spreadRadius: widgetFit.unit * 4),
              ]
          ),
          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
          // 随机生成高度
          height: widgetFit.unit * 390,
          child: GestureDetector(
            child: Container(
              // alignment: Alignment.center,
              child: Column(
                children: <Widget>[
                  Container(
                    width:
                    MediaQuery.of(context).size.width,
                    height: widgetFit.unit * 250,
                    decoration: GoodsList[index]['image'] !=
                        null &&
                        GoodsList[index]['image'] != ''
                        ? BoxDecoration(
                        image: DecorationImage(
                            image: NetworkImage(
                                Compatible.Http(
                                    GoodsList[index]
                                    ['image'])),
                            fit: BoxFit.fitWidth),
                        borderRadius:
                        BorderRadius.circular(
                            widgetFit.unit * 12))
                        : BoxDecoration(),
                    // color: Colors.black45,
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(
                        widgetFit.unit * 16,
                        widgetFit.unit * 10,
                        widgetFit.unit * 16,
                        0),
                    alignment: Alignment.centerLeft,
                    width:
                    MediaQuery.of(context).size.width -
                        widgetFit.unit * 32,
                    child: Text(
                      GoodsList[index]['goodsName'] !=
                          null &&
                          GoodsList[index]
                          ['goodsName'] !=
                              ''
                          ? '${GoodsList[index]['goodsName']}'
                          : '',
                      style: TextStyle(
                          color: Color(0xFF333333),
                          fontSize: widgetFit.unit * 30,
                          fontWeight: FontWeight.w500),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(
                        widgetFit.unit * 16,
                        widgetFit.unit * 10,
                        widgetFit.unit * 16,
                        0),
                    alignment: Alignment.centerLeft,
                    width:
                    MediaQuery.of(context).size.width -
                        widgetFit.unit * 32,
                    child: Text(
                        GoodsList[index]['price'] != null &&
                            GoodsList[index]['price'] !=
                                ''
                            ? '￥${GoodsList[index]['price']}'
                            : '',
                        style: TextStyle(
                            color: Color(0xFFFF8527),
                            fontSize: widgetFit.unit * 30,
                            fontWeight: FontWeight.w500),
                        overflow: TextOverflow.ellipsis),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(
                        widgetFit.unit * 16,
                        widgetFit.unit * 10,
                        widgetFit.unit * 16,
                        0),
                    alignment: Alignment.centerLeft,
                    width:
                    MediaQuery.of(context).size.width -
                        widgetFit.unit * 32,
                    child: Row(
                      children: <Widget>[
                        Container(
                          child: Image.asset(
                            'assets/image/service_two/company@2x.png',
                            width: widgetFit.unit * 24,
                            height: widgetFit.unit * 21,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(
                              widgetFit.unit * 8, 0, 0, 0),
                          child: Text(
                              GoodsList[index][
                              'production'] !=
                                  null &&
                                  GoodsList[index]
                                  [
                                  'production'] !=
                                      ''
                                  ? '${GoodsList[index]['production']}'
                                  : '',
                              style: TextStyle(
                                  color: Color(0xFF999999),
                                  fontSize:
                                  widgetFit.unit * 24),
                              overflow:
                              TextOverflow.ellipsis),
                        )
                      ],
                    ),
                  )
                ],
              ),
              width: widgetFit.unit * 342,
              height: widgetFit.unit * 508,
              decoration: BoxDecoration(
                  color: Color(0xFFFFFFFF),
                  borderRadius: BorderRadius.circular(
                      widgetFit.unit * 10)),
            ),
            
          ),
        )
    );
  }
//  精选需求 item
  initDemandItem(item) {
    return item!=null?GestureDetector(
      
      child: Container(
        margin: EdgeInsets.fromLTRB(10,widgetFit.unit * 20,10,0),
        padding: EdgeInsets.fromLTRB(widgetFit.unit * 32, widgetFit.unit * 32,
            widgetFit.unit * 32, widgetFit.unit * 0),
        alignment: Alignment.topLeft,
        width: MediaQuery.of(context).size.width-20,
        height: widgetFit.unit * 255,
        decoration: BoxDecoration(
          color: Color(0xFFFFFFFF),
          borderRadius: BorderRadius.circular(widgetFit.unit * 10),
          boxShadow: [
            BoxShadow(
                color: Color(0xFFF0F0F0),
                offset: Offset(1.0, 2.0),
                blurRadius: widgetFit.unit * 2.0,
                spreadRadius: widgetFit.unit * 4),
          ],

        ),
        child: Column(
          children: <Widget>[
            Container(
              height: widgetFit.unit * 58,
              child: Row(
                children: <Widget>[
                  Container(
                    width: widgetFit.unit * 400,
                    child: Text('${item['name']}',//item['name']
                        style: TextStyle(
                            fontSize: widgetFit.unit * 32,
                            fontWeight: FontWeight.w500)),
                  ),
                  Container(
                      width: widgetFit.unit * 238,
                      alignment: Alignment.centerRight,
                      child: RichText(
                        text: TextSpan(
                          text: '￥',
                          style: TextStyle(
                              color: Color(0XFFFF581E),
                              fontSize: widgetFit.unit * 28),
                          children: <TextSpan>[
                            TextSpan(
                                text: '${item['dayRentHigh']}',
                                style: TextStyle(
                                    color: Color(0XFFFF581E),
                                    fontSize: widgetFit.unit * 36)),
                            TextSpan(
                                text: '/天',
                                style: TextStyle(
                                    color: Color(0XFF8E9198),
                                    fontSize: widgetFit.unit * 28))
                          ],
                        ),
                      ))
                ],
              ),
            ),
            Container(
              height: widgetFit.unit * 58,
              child: Row(
                children: <Widget>[
                  Container(
                    width: widgetFit.unit * 500,
                    child: Row(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.fromLTRB(
                              0, widgetFit.unit * 6, 0, 0),
                          color: Color(0xFFF3F3F3),
                          padding: EdgeInsets.fromLTRB(
                              widgetFit.unit * 10,
                              widgetFit.unit * 5,
                              widgetFit.unit * 10,
                              widgetFit.unit * 5),
                          child: Text('租金高',
                              style: TextStyle(
                                  color: Color(0XFF797F8B),
                                  fontSize: widgetFit.unit * 22)),
                        )
                      ],
                    ),
                  ),
                  Container(
                      width: widgetFit.unit * 138,
                      alignment: Alignment.centerRight,
                      child: RichText(
                        text: TextSpan(
                          text: -1 == -1
                              ? '随租随售'
                              : '${item['leaseTerm']}' + '个月起租',
                          style: TextStyle(
                              color: Color(0XFF8E9198),
                              fontSize: widgetFit.unit * 26),
                        ),
                      ))
                ],
              ),
            ),
            Container(
              // color: Colors.red,
              height: widgetFit.unit * 98,
              child: Row(
                children: <Widget>[
                 Container(
                   // color: Colors.black45,
                   width: widgetFit.unit * 64,
                   height: widgetFit.unit * 64,
                   decoration:
                   item['headImage'] != null && item['headImage'] != ''
                       ? BoxDecoration(
                     color: Color(0xFFFEFEFE),
                     image: DecorationImage(
                         image: NetworkImage(
                             Compatible.Http(item['headImage'])),
                         fit: BoxFit.fill),
                   )
                       : BoxDecoration(color: Color(0xFFFEFEFE)),
                 ),
                  Container(
                    margin: EdgeInsets.fromLTRB(widgetFit.unit * 24, 0, 0, 0),
                    child: Text(
                      '${item['businessName']}',
                      style: TextStyle(
                          fontSize: widgetFit.unit * 24,
                          color: Color(0xFF313948)),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    ):Container();
  }





  Widget buildBody(BuildContext context) {
    final double topPadding = MediaQuery.of(context).padding.top;
    final double bottomPadding = MediaQuery.of(context).padding.bottom;
    return new Scaffold(
          appBar: new AppBar(
            elevation: 0.01,
            leading: null,
            centerTitle: false,
            automaticallyImplyLeading: false,
            backgroundColor: Colors.white,
            title: buildSearchBox(),
          ),
          body: SafeArea(
              bottom: false,
              child: Container(
                 color: Colors.white,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  padding: EdgeInsets.fromLTRB(0,0, 0, 0),
                  child: new Refresh(
                      onHeaderRefresh: onHeaderRefresh,
                      childBuilder: (BuildContext context, {ScrollController controller, ScrollPhysics physics}) {
                        return SingleChildScrollView(
                          controller: controller,
                          child: Column(
                            children: <Widget>[
                              buildActions(),
                              buildBanner(),
                              buildNotice(),
                              buildSelectGoods(),
                              buildSelectDemand(),

                              Container(
                                color: Colors.white,
                                width: MediaQuery.of(context).size.width,
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                       padding: EdgeInsets.fromLTRB(17,0, 0,0),
                                       width: MediaQuery.of(context).size.width,
                                       height: 50.0,
                                       child: Row(
                                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                         children: <Widget>[
                                           Text('了解麻利Nowle',style: new TextStyle(fontSize: widgetFit.unit * 36,fontWeight: FontWeight.w500),),
                                          
                                          
                                         ],
                                       ),
                                     ),
                                    UnderStandNowleList!=null?Container(
                                      margin: EdgeInsets.fromLTRB(widgetFit.unit * 10, widgetFit.unit * 20, widgetFit.unit * 10, widgetFit.unit * 40),
                                      child:Wrap(
                                            spacing: widgetFit.unit * 0,
                                            runSpacing: widgetFit.unit * 40,
                                            alignment: WrapAlignment.start,
                                            children: worker<Widget>(UnderStandNowleList,
                                                (index, item) {
                                        return GestureDetector(
                                          child:Container(
                                            color:Colors.white,
                                          width: widgetFit.unit * 364,
                                            child:Row(children: <Widget>[
                                              Container(
                                                width: widgetFit.unit * 68,
                                                height: widgetFit.unit * 68,
                                                
                                                margin: EdgeInsets.fromLTRB(widgetFit.unit * 20, widgetFit.unit * 0, 0, 0),
                                                decoration: UnderStandNowleList[index]['image'] !=
                                                    null &&
                                                UnderStandNowleList[index]['image'] != ''
                                            ? BoxDecoration(
                                                image: DecorationImage(
                                                    image: NetworkImage(
                                                        Compatible.Http(
                                                            UnderStandNowleList[index]
                                                                ['image'])),
                                                    fit: BoxFit.fill),
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        widgetFit.unit * 12))
                                            : BoxDecoration(),
                                              ),
                                              Container(
                                                width: widgetFit.unit * 274,
                                                alignment: Alignment.centerLeft,
                                                padding: EdgeInsets.fromLTRB(widgetFit.unit * 27, 0, 0, 0),
                                                child:Column(children: <Widget>[
                                                  Container(
                                                    width: widgetFit.unit * 274,
                                                alignment: Alignment.centerLeft,
                                                child: Text('${UnderStandNowleList[index]['title']}',overflow: TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: widgetFit.unit * 30,fontWeight: FontWeight.w500)
                                                  
                                                ),
                                              ),
                                              Container(
                                                width: widgetFit.unit * 274,
                                                alignment: Alignment.centerLeft,
                                                padding: EdgeInsets.fromLTRB(0, widgetFit.unit * 10, 0, 0),
                                                child: Text('${UnderStandNowleList[index]['remark']}',overflow: TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: widgetFit.unit * 24),
                                                  
                                                ),
                                              )
                                                ],)
                                              )
                                            ])
                                          ),
                                          onTap: ()=>{
                                            UnderStandNowleList[index]['url']!=null&&UnderStandNowleList[index]['url']!=''?Navigator.of(context)
                                                .push(new MaterialPageRoute(builder: (context) {
                                              return new WebviewBrowser(title: "",url: UnderStandNowleList[index]['url'],);
                                            })):false,
                                          },
                                            );}),
                                        
                                      )
                                    ):Container()
                                          
                                  ],
                                ),
                              ),
                              
                            ],
                          ),
                        );
                      })))
    );
  }

  @override
  Widget build(BuildContext context) {
    this.controllerHeader = new AnimationController(vsync: this);
    this.controllerFooter = new AnimationController(vsync: this);

    //  pullInfoanimation = new Tween<double>(begin: 0.0, end:100.0).animate(pullInfocontroller);
    //  pullInfocontroller.forward();
    //  UserIdentificationBool = true;
    return buildBody(context);

    /*
    return SafeArea(
        child: Stack(
      children: <Widget>[
        new Column(
          children: <Widget>[
            // SearchBar
            buildSearchBox(),
            //
            buildActions(),
            // TabBar
            buildTabBar(),
            // buildNotice(),
            // Build Content
          ],
        ),
        Positioned(
            top: 0,//widgetFit.unit * 180 + 100,
            left: 0,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Container(
                color: Color(0xFFF8F8F8),
                child:
                    GoodsList.length > 0 ? buildGoodsList() : buildEmptyBox())),
      ],
    ));
    */
  }
}
