import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_gifimage/flutter_gifimage.dart';
import 'package:my_app/net/WebviewBrowser.dart';
import 'package:my_app/request/request.dart';
import 'package:url_launcher/url_launcher.dart';
import '../first/widgetFit.dart';
import '../net/Api.dart';
import 'package:package_info/package_info.dart';

class Home extends StatefulWidget {
  final int id;
  Home({this.id});
  @override
  State<StatefulWidget> createState() => _HomeState();
}

class _HomeState extends State<Home> with TickerProviderStateMixin {
  GifController controller1, controller2, controller3, controller4, controller5;

  @override
  void initState() {
    super.initState();
    controller1 = GifController(vsync: this);
    controller2 = GifController(vsync: this);
    controller3 = GifController(vsync: this);
    controller4 = GifController(vsync: this);
    controller5 = GifController(vsync: this);
    controller1.value = 0;
    controller1.animateTo(24, duration: Duration(milliseconds: 50));
    checkUpdateVersion();
    setState(() {
      id = widget.id;
    });

    if (Platform.isIOS) {
      //ios相关代码
      SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top]);
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
    } else if (Platform.isAndroid) {
      // android相关代码
      print("android");
    }
  }

  // 检查是否有更新
  void checkUpdateVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String version = packageInfo.version;
    String buildNumber = packageInfo.buildNumber;
    var url = Api.UPDATE_URL +
        '/app/tuanyou.json?version=${version}&buildNumber=${buildNumber}';
    print(url);
    var versionNumber = int.parse(version.replaceAll(".", ""));
    var data = await Request().get(url);
    if (data != null) {
      var androidNumber = data['android_num'];
      var iosNumber = data['ios_num'];
      var fourceUpdate = data['fource_update'];
      var downloadUrl = data["content_url"];
      var iosDownloadUrl = data["appstore_url"];
      var androidDownloadUrl = data["android_url"];
      print("当前版本${buildNumber},线上版本Android:${androidNumber},IOS:${iosNumber}");
      if (Platform.isIOS) {
        if (iosNumber > int.parse(buildNumber)) {
          print("ISO有更新");
          _showAlert(iosDownloadUrl,
              fourceUpdate: fourceUpdate == 1 ? true : false);
        }
      } else if (Platform.isAndroid) {
        if (androidNumber > int.parse(buildNumber)) {
          print("Android有更新");
          _showAlert(androidDownloadUrl,
              fourceUpdate: fourceUpdate == 1 ? true : false);
        }
      } else if (version != data['version'] &&
          versionNumber < int.parse(data['version'].replaceAll(".", ""))) {
        _showAlert(downloadUrl, fourceUpdate: fourceUpdate == 1 ? true : false);
      }
    }
    /*
    Request().get(url).then((data) {
      print(data);
      try {
        var androidNumber = data['android_num'];
        var iosNumber = data['ios_num'];
        var fourceUpdate = data['fource_update'];
        var downloadUrl = data["content_url"];
        var iosDownloadUrl = data["appstore_url"];
        var androidDownloadUrl = data["android_url"];
        print(
            "当前版本${buildNumber},线上版本Android:${androidNumber},IOS:${iosNumber}");
        if (Platform.isIOS) {
          if (iosNumber > int.parse(buildNumber)) {
            print("ISO有更新");
            _showAlert(iosDownloadUrl,
                fourceUpdate: fourceUpdate == 1 ? true : false);
          }
        } else if (Platform.isAndroid) {
          if (androidNumber > int.parse(buildNumber)) {
            print("Android有更新");
            _showAlert(androidDownloadUrl,
                fourceUpdate: fourceUpdate == 1 ? true : false);
          }
        } else if (version != data['version'] &&
            versionNumber < int.parse(data['version'].replaceAll(".", ""))) {
          _showAlert(downloadUrl,
              fourceUpdate: fourceUpdate == 1 ? true : false);
        }
      } catch (e) {
        print(e);
      }
    });
    */
  }

  Future<void> _showAlert(_content_url, {bool fourceUpdate: false}) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            title: Text(
              '有新版本更新哟~',
              style: TextStyle(
                  color: Color(0XFF666666), fontSize: widgetFit.unit * 30),
            ),
            actions: <Widget>[
              new CupertinoButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    '取消',
                    style: TextStyle(
                        color: Color(0XFF666666),
                        fontSize: widgetFit.unit * 30),
                  )),
              new CupertinoButton(
                  onPressed: () async {
                    if (Platform.isIOS) {
                      if (await canLaunch(_content_url)) {
                        await launch(_content_url, forceSafariVC: false);
                      } else {
                        throw 'Could not launch $_content_url';
                      }
                    } else if (Platform.isAndroid) {
                      if (await canLaunch(_content_url)) {
                        await launch(_content_url, forceSafariVC: false);
                      } else {
                        throw 'Could not launch $_content_url';
                      }
                    } else {
                      Navigator.of(context)
                          .push(new MaterialPageRoute(builder: (context) {
                        return new WebviewBrowser(
                            url: _content_url, title: "麻利Nowle");
                      }));
                    }
                  },
                  child: Text(
                    '立即更新',
                    style: TextStyle(
                        color: Color(0XFFFF8527),
                        fontSize: widgetFit.unit * 30),
                  )),
            ],
          );
        });
  }

  var id = 5;

  int _currentIndex = 0;
  final List<Widget> _children = [

  ];

  void _onTapHandler(int index) {
    if (index == 0) {
      controller1.value = 0;
      controller1.animateTo(24, duration: Duration(milliseconds: 1000));
    } else if (index == 3) {
      controller2.value = 0;
      controller2.animateTo(41, duration: Duration(milliseconds: 1000));
    } else if (index == 2) {
      controller3.value = 0;
      controller3.animateTo(19, duration: Duration(milliseconds: 1000));
      
    } else if (index == 1) {
      controller4.value = 0;
      controller4.animateTo(24, duration: Duration(milliseconds: 1000));
    } else {
      controller5.value = 0;
      controller5.animateTo(56, duration: Duration(milliseconds: 1000));
    }
    if (index == 6) {
      // ios相关代码
      if (Platform.isIOS) {
        SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top]);
      }
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    } else {
      // ios相关代码
      if (Platform.isIOS) {
        SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top]);
      }
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
    }
    setState(() {
      id = null;
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    widgetFit.unit = MediaQuery.of(context).size.width / 750;
    return Scaffold(
        // backgroundColor: Colors.white,
        body: IndexedStack(index: _currentIndex, children: _children),
        // body: _children[id != null ? id : _currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          selectedFontSize: widgetFit.unit * 20,
          unselectedFontSize: widgetFit.unit * 20,
          currentIndex: id != null ? id : _currentIndex,
          type: BottomNavigationBarType.fixed,
          onTap: _onTapHandler,
          fixedColor: Color(0xFFFF8527),
          backgroundColor: Colors.white,
          items: [
            BottomNavigationBarItem(
                icon: Image.asset(
                  'assets/image/service_two/tab_1@2x.png',
                  width: widgetFit.unit * 44,
                  height: widgetFit.unit * 44,
                ),
                activeIcon: GifImage(
                  width: widgetFit.unit * 44,
                  height: widgetFit.unit * 44,
                  controller: controller1,
                  image: AssetImage("assets/image/service_two/tab1.gif"),
                ),
                title: Container(
                  padding: EdgeInsets.fromLTRB(0, widgetFit.unit * 8, 0, 0),
                  child: Text('首页'),
                )),
                BottomNavigationBarItem(
                icon: Image.asset(
                  'assets/image/service_two/tab_2@2x.png',
                  width: widgetFit.unit * 44,
                  height: widgetFit.unit * 44,
                ),
                activeIcon: GifImage(
                  width: widgetFit.unit * 44,
                  height: widgetFit.unit * 44,
                  controller: controller2,
                  image: AssetImage("assets/image/service_two/tab2.gif"),
                ),
                title: Container(
                  padding: EdgeInsets.fromLTRB(0, widgetFit.unit * 8, 0, 0),
                  child: Text('发现'),
                )),
            BottomNavigationBarItem(
                icon: Image.asset(
                  'assets/image/service_two/tab_4@2x.png',
                  width: widgetFit.unit * 44,
                  height: widgetFit.unit * 44,
                ),
                activeIcon: GifImage(
                  width: widgetFit.unit * 44,
                  height: widgetFit.unit * 44,
                  controller: controller4,
                  image: AssetImage("assets/image/service_two/tab4.gif"),
                ),
                title: Container(
                  padding: EdgeInsets.fromLTRB(0, widgetFit.unit * 8, 0, 0),
                  child: Text('租赁市场'),
                )),
            BottomNavigationBarItem(
                icon: Image.asset(
                  'assets/image/service_two/tab_3@2x.png',
                  width: widgetFit.unit * 44,
                  height: widgetFit.unit * 44,
                ),
                activeIcon: GifImage(
                  width: widgetFit.unit * 44,
                  height: widgetFit.unit * 44,
                  controller: controller3,
                  image: AssetImage("assets/image/service_two/tab3.gif"),
                ),
                title: Container(
                  padding: EdgeInsets.fromLTRB(0, widgetFit.unit * 8, 0, 0),
                  child: Text('客服'),
                )),
            
            BottomNavigationBarItem(
                icon: Image.asset(
                  'assets/image/service_two/tab_5@2x.png',
                  width: widgetFit.unit * 44,
                  height: widgetFit.unit * 44,
                ),
                activeIcon: GifImage(
                  width: widgetFit.unit * 44,
                  height: widgetFit.unit * 44,
                  controller: controller5,
                  image: AssetImage("assets/image/service_two/tab5.gif"),
                ),
                title: Container(
                  padding: EdgeInsets.fromLTRB(0, widgetFit.unit * 8, 0, 0),
                  child: Text('我的'),
                ))
          ],
        ));
  }
}
